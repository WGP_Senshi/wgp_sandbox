
private ["_classname"];
_classname = "";



switch (true) do {
    case (isClass (configfile >> "CfgWeapons" >> _this)) : {_classname = configfile >> "CfgWeapons" >> _this};
    case (isClass (configfile >> "CfgVehicles" >> _this)) : {_classname = configfile >> "CfgVehicles" >> _this};
    case (isClass (configfile >> "CfgMagazines" >> _this)) : {_classname = configfile >> "CfgMagazines" >> _this};
    default {_classname = nil };
};
_classname
