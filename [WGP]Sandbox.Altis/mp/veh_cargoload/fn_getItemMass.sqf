/*
    Author: Fett_Li
    
    Description:
        This script returns the "room" an item/weapon takes.
    
    Parameter(s):
        _this select 0:
        STRING - item
        
    Returns:
        NUMBER - mass of the item
*/
private ["_item", "_mass"];
_item = _this select 0;
_mass = 0;

if (isClass (configfile >> "CfgWeapons" >> _item)) then {
    if (isClass (configfile >> "CfgWeapons" >> _item >> "weaponSlotsInfo")) then {
        _mass = getNumber (configfile >> "CfgWeapons" >> _item >> "weaponSlotsInfo" >> "mass");
    } else {
        _mass = getNumber (configfile >> "CfgWeapons" >> _item >> "itemInfo" >> "mass")
    };
} else {
    if (isClass (configfile >> "CfgMagazines" >> _item)) then {
        _mass = getNumber (configfile >> "CfgMagazines" >> _item >> "mass");
    } else {
        _mass = getNumber (configfile >> "CfgVehicles" >> _item >> "mass");
    };
};
_mass