/*
    Author: [W] Fett_Li
    Modified by [WGP]Senshi
    
    Description:
        This script returns the space a vest/uniform/backpack has.
    
    Parameter(s):
        _this select 0: 
        STRING - objectname of the vest/uniform/backpack
        
    Returns:
        NUMBER - space of the vest/uniform/backpack
*/

_item = _this select 0;
_itemname = typeof _item;

_cargo = 0;
_array = [];

if (isClass (configFile >> "CfgVehicles" >> _itemname)) then {
    _cargo = getNumber (configFile >> "cfgVehicles" >> _itemname >> "maximumLoad");
} else {
    _array = toArray getText (configFile >> "CfgWeapons" >> _itemname >> "iteminfo" >> "containerClass");
    for "_i" from 6 to (count _array -1) do {
        _cargo=_cargo+10^(count _array - 1 - _i)*((_array select _i)-48)
    };
};
_cargo