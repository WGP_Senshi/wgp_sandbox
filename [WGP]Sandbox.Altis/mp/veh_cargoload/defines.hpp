#define GRID_ABS_W		1
#define GRID_ABS_H		1
#define GRID_TITLE_H 	0.05
#define GRID_COUNT_W 	40
#define GRID_COUNT_H 	20

/* Automatisch zentriert berechnen */
#define GRID_W			(GRID_ABS_W / GRID_COUNT_W)
#define GRID_H			(GRID_ABS_H / GRID_COUNT_H)
#define GRID_ABS_X		(0)
#define GRID_ABS_X_RIGHT (GRID_ABS_W / 2 + GRID_ABS_W)
#define GRID_ABS_Y		(0)



/* IDs 900-950 */
#define WGP_VEH_CargoLoad_IDD							900
#define WGP_VEH_CargoLoad_IDC_MassBar					901
#define WGP_VEH_CargoLoad_IDC_Title 					902
#define WGP_VEH_CargoLoad_IDC_ButtonOK					903
#define WGP_VEH_CargoLoad_IDC_ButtonCancel 				904
#define WGP_VEH_CargoLoad_LISTNBOX_RIGHT_IDC 			905
#define WGP_VEH_CargoLoad_LISTNBOX_LEFT_IDC 			906
#define WGP_VEH_CargoLoad_LISTNBOX_IDC		 			907