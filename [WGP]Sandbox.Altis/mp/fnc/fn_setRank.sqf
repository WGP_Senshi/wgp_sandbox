private["_id","_newrank"];

_id = _this select 0;
_newrank = _this select 1;
// diag_log format["DEBUG: Rank change: " + _newrank];

_id setRank _newrank;

true