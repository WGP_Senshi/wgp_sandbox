while {true} do {
	{
        _dead = _x;
        {
		    if ([_x, _dead] call bis_fnc_intrigger) then {
		        deletevehicle _dead;
		    };
        } foreach _this;
	} foreach alldead;
    
    {
        _class = _x;
		{
		    _unit = _x;
	        {
	            if ([_x, _unit] call bis_fnc_intrigger) then {
	                // player sidechat (str _x + " " + str _unit);
	            	deletevehicle _unit;
	        	};
	    	} foreach _this;
		} forEach allMissionObjects _x;
    } foreach [
	     //"GroundWeaponHolder",
	     "WeaponHolderSimulated",
	     "CraterLong",
	     //"StaticWeapon",
	     "Default"
	     //"Ruins"
	     //"EmptyDetector",
	     //"#smokesource",
	     //"#destructioneffects"
 	];
    sleep 5;
};

true