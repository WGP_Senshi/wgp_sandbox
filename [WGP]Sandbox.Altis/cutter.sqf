waitUntil {not(isNil "BIS_fnc_init")};
_start = getpos  (_this select 0);
_xpos = _this select 1;
_ypos = _this select  2;
_triggers = _this select 3;
for [{_xx = 0},{_xx < _xpos},{_xx = _xx + 15}] do {
	_random = round (random 5);
    for [{_yy = 0},{_yy < _ypos},{_yy = _yy + 15}] do {
		_inside = false;
		_newpos = [(_start select 0) +_xx + round (random 10),(_start select 1)+_yy + round (random 10),0];
		{ if ([_x, _newpos] call BIS_fnc_inTrigger) then {
			_inside = true}
		} foreach _triggers;
		
		if (_inside) then {
			createVehicle ["ClutterCutter_EP1",_newpos,[], 0, "can_collide"];
		};
	};
};