comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_B_GhillieSuit";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
this addItemToUniform "AGM_EarBuds";
this addItemToUniform "SmokeShellBlue";
for "_i" from 1 to 3 do {this addItemToUniform "9Rnd_45ACP_Mag";};
this addVest "V_Chestrig_khk";
for "_i" from 1 to 8 do {this addItemToVest "7Rnd_408_Mag";};

comment "Add weapons";
this addWeapon "srifle_LRR_camo_F";
this addPrimaryWeaponItem "optic_LRPS";
this addWeapon "hgun_ACPC2_F";
this addWeapon "Binocular";

this addItemToVest "7Rnd_408_Mag";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "tf_anprc152";