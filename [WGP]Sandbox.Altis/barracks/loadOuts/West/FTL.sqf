comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_B_CTRG_1";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
this addItemToUniform "AGM_EarBuds";
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addVest "V_PlateCarrierH_CTRG";
for "_i" from 1 to 6 do {this addItemToVest "30Rnd_65x39_caseless_mag";};
for "_i" from 1 to 2 do {this addItemToVest "30Rnd_65x39_caseless_mag_Tracer";};
this addItemToVest "3Rnd_Smoke_Grenade_shell";
for "_i" from 1 to 4 do {this addItemToVest "3Rnd_HE_Grenade_shell";};
this addHeadgear "H_HelmetB_plain_mcamo";

comment "Add weapons";
this addWeapon "arifle_MX_GL_Black_F";
this addPrimaryWeaponItem "acc_pointer_IR";
this addPrimaryWeaponItem "optic_Aco";
this addWeapon "Rangefinder";

this addItemToVest "30Rnd_65x39_caseless_mag";
this addItemToVest "3Rnd_HE_Grenade_shell";

comment "Add items";
this linkItem "GPS";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "tf_anprc152";