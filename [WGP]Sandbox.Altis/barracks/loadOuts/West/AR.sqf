comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_B_CTRG_1";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addItemToUniform "AGM_EarBuds";
this addVest "V_PlateCarrierH_CTRG";
for "_i" from 1 to 7 do {this addItemToVest "100Rnd_65x39_caseless_mag_Tracer";};
this addHeadgear "H_HelmetB_plain_mcamo";

comment "Add weapons";
this addWeapon "arifle_MX_SW_Black_F";
this addPrimaryWeaponItem "acc_pointer_IR";
this addPrimaryWeaponItem "optic_Aco";
this addPrimaryWeaponItem "bipod_01_F_blk";
this addWeapon "Binocular";

this addItemToVest "100Rnd_65x39_caseless_mag_Tracer";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "tf_anprc152";