comment "Exported from Arsenal by [WGP]Senshi";

comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "BWA3_Uniform_Fleck";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
this addItemToUniform "AGM_EarBuds";
for "_i" from 1 to 2 do {this addItemToUniform "BWA3_DM25";};
this addVest "BWA3_Vest_Rifleman1_Fleck";
for "_i" from 1 to 8 do {this addItemToVest "BWA3_30Rnd_556x45_G36";};
this addItemToVest "BWA3_DM51A1";
this addBackpack "B_AssaultPack_rgr";
this addItemToBackpack "BWA3_Pzf3_IT";
this addHeadgear "BWA3_M92_Fleck";

comment "Add weapons";
this addWeapon "BWA3_G36";
this addPrimaryWeaponItem "BWA3_acc_LLM01_flash";
this addPrimaryWeaponItem "BWA3_optic_RSAS";
this addWeapon "BWA3_Pzf3";
this addWeapon "Binocular";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "tf_anprc152_10";
this linkItem "ItemGPS";

removeBackpack this;
