comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "rhs_uniform_vdv_emr";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
for "_i" from 1 to 2 do {this addItemToUniform "rhs_mag_rdg2_white";};
this addItemToUniform "AGM_EarBuds";
this addVest "rhs_6b23_digi";
this addItemToVest "rhs_mag_rgd5";
this addBackpack "rhs_sidor";
for "_i" from 1 to 3 do {this addItemToBackpack "rhs_100Rnd_762x54mmR_green";};
this addHeadgear "rhs_6b27m_green_ess";

comment "Add weapons";
this addWeapon "rhs_weap_pkp";
this addWeapon "Binocular";

this addItemToBackpack "rhs_100Rnd_762x54mmR_green";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "tf_fadak";
this linkItem "ItemWatch";