#define GRID_ABS_W		1
#define GRID_ABS_H		0.5
#define GRID_TITLE_H 	0.05
#define GRID_COUNT_W 	100
#define GRID_COUNT_H 	100

/* Automatisch zentriert berechnen */
#define GRID_W			(GRID_ABS_W / GRID_COUNT_W)
#define GRID_H			(GRID_ABS_H / GRID_COUNT_H)
#define GRID_ABS_X		(0.5 - (GRID_ABS_W / 2))
#define GRID_ABS_Y		(0.5 - (GRID_ABS_H / 2) - 0.2)

/* ID's 500-549 */
#define PIXLOGISTIC_IDD_DIALOG_ADMIN			500
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Title 		501
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Title_1	525
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button1_1 	502
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button1_2  522
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button1_3  523
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button1_4  524
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button2 	503
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button3 	504
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button4	505
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button5 	506
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button6 	507
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button7 	508
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button8 	509
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button9 	510
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button10	511
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button11	512
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button12	513
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button13	514
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button14	515
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button15	516
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button16	517
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button17	518
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button18	519
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button19	520
#define PIXLOGISTIC_IDC_DIALOG_ADMIN_Button20	521