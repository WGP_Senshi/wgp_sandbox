private ["_unit", "_doorstate", "_unit", "_alt", "_speed"];
_unit = _this; 
_doorstate = false;

_autodoors = [
	["Heli_Transport_01_base_F", ["door_R", "door_L"]],
	["Heli_Transport_02_base_F", ["Door_Back_L", "Door_Back_R", "CargoRamp_Open"]],
    ["Heli_Transport_03_base_F", ["Door_L_source", "Door_R_source", "Door_rear_source"]],
    ["Heli_Transport_04_base_F", ["Door_1_source", "Door_2_source", "Door_3_source"]],
    ["Heli_Attack_02_base_F", ["door_L", "door_R"]]
];

{
	if (_unit iskindOf (_x select 0))
} foreach _autodoors;

while {alive _unit} do {
    if (count crew _unit > 0 && local _unit) then {
		_alt = getposvisual _unit select 2;
		_speed = sqrt ( (velocity _unit select 0)^2 + (velocity _unit select 1)^2 + (velocity _unit select 2)^2 );
	    //systemchat (str(_speed) + " / " + str(_unit getSpeed "slow") + " -- " + str(_alt));
		// If under 10 altitude and doors are already closed, open them.
		if ((_alt <= 10) && _speed < _unit getSpeed "slow") then {
	        if !(_doorState) then {
	            {
	                if (_unit iskindOf (_x select 0)) then {
	                    {
	                    	_unit animateDoor [_x, 1];
	                    } foreach(_x select 1);
	                }
	            } foreach _autodoors;
				_doorstate = true;
	        }
		} else {
	        if (_doorstate) then {
			// If over 10 altitude and doors are open -> close them.
	            {
	                if (_unit iskindOf (_x select 0)) then {
	                    {
	                    	_unit animateDoor [_x, 0];
	                    } foreach(_x select 1);
	                }
	            } foreach _autodoors;
				_doorstate = false;
			};
	    };
	};
	sleep 1;
};