class AAS
{
	tag = "AAS";
	class AAS
	{
		file = "functions\AAS";
		class initVehicle {description = "WGP AAS: Initializes vehicle for AAS";}; // Function name:  AAS_fnc_initVehicle
		class respawnVehicle {description = "WGP AAS: Initializes vehicle for AAS";}; // Function name:  AAS_fnc_respawnVehicle
		class addTickets {description = "WGP AAS: Adds tickets";}; // Function name:  AAS_fnc_addTickets
		class onPlayerConnected {description = "WGP AAS: JIP Sync";}; // Function name:  AAS_fnc_initVehicle
	};
	
	class resupply
	{
		file = "functions\resupply";
		class resupply {description = "WGP AAS: Service vehicle";};
	}
};
class WGP
{
    tag = "WGP";
    class util
    {
        file = "functions\util";
        class findInString {description = "KK Util: Finds String within string";};
        class uniquearray {description = "KK Util: Returns copy of array without duplicate elements";};
        class arrayaverage {description = "WGP Util: Returns arithmetic average of array";};
        class findClass {description = "WGP Util: Find and return class of given object";};
        class str_formatWeight {description = "WGP Util: Converts abstract item mass to kg or lb format";};
    };
    class squad
    {
        file = "functions\squad";
		class setRank {description = "WGP MP: Sets Rank";}; // Function name:  wgp_fnc_setRank
		class setGroupID {description = "WGP MP: Sets group ID";}; // Function name:  wgp_fnc_setGroupID
    };
    class kits
    {
        file = "functions\kits";
		class getKitcounts {description = "WGP: gets counts of kits in team and squad";}; // Function name:  wgp_fnc_setRank
		class getKitIcon {description = "WGP: gets appropiate kit icon";}; // Function name:  wgp_fnc_setRank
    };
	class misc
	{
		file = "functions\misc";
		class misc_playRadio {description = "WGP AAS: Displays AAS message";}; // Function name:  wgp_fnc_misc_playRadio
		class disablethermals {description = "WGP AAS: LOOP - Monitors thermals disable";}; // Function name:  hud_fnc_disablethermals
	};
	class hud
	{
		file = "functions\hud";
		class hud_showMessage {description = "WGP AAS: Displays AAS message";}; // Function name:  wgp_fnc_hud_showMessage
	};
};