if (!isServer) exitWith{};
private ["_netid", "_clientid"];
_netid = _this select 0;
_clientId = -1;

sleep 1;
while{_clientId == -1} do {
    // Find NetID of JIP player so we know where to send the data
	{
		if (owner _x == _netid) exitWith
		{
            // diag_log ("JIP: Found a matching clientID: " + str(owner _x));
			_clientId = owner _x;
		};
	} forEach playableUnits;
};

diag_log ("JIP DEBUG: Updating values...");
{
	private ["_group", "_CustomGroup"];
	_group = group _x;
	_CustomGroup = _group getVariable "CustomGroup";
	if (!isNil "_CustomGroup") then {
		pV_changeGroupID = [_group, _CustomGroup]; _clientID publicVariableClient "pV_changeGroupID";
	};
    pV_changeRank = [_x, rank _x]; _clientID publicVariableClient "pV_changeRank";
    pV_playerkitID = [_x, _x getVariable "playerKitID"]; _clientID publicVariableClient "pV_playerkitID";
} foreach playableUnits;

pV_setTickets = [west, p_AAS_Tickets_West]; _clientID publicVariableClient "pV_setTickets";
pV_setTickets = [east, p_AAS_Tickets_East]; _clientID publicVariableClient "pV_setTickets";

{
    _trigger = _x select 0;
    pV_updateflag = [_foreachindex, _trigger getVariable "side", _trigger getVariable "sideattack", _trigger getVariable "CapPoints"];
    publicvariable "pV_updateflag";
} foreach pV_bases;
//pV_AAS_refreshMarkers = true; _clientID publicVariableClient "pV_AAS_refreshMarkers";


true