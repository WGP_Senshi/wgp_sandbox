/* Respawns a vehicle after a given time at a specific location
 * 
 * _this = vehicle
 * 
 */
if !isServer exitwith {};

private ["_veh", "_pos", "_dir", "_delay", "_side", "_vehclass", "_tickets", "_name", "_vehtype", "_newveh"];
_veh		= _this;
_pos		= _veh getVariable "pos";
_dir		= _veh getVariable "dir";
_delay		= _veh getVariable "delay";
_side		= _veh getVariable "side";
_vehclass	= _veh getVariable "vehclass";
_name		= getText (configFile >> "CfgVehicles" >> typeof _veh >> "displayname" );
_vehtype	= typeof _veh;

uiSleep _delay;
deletevehicle _veh;
uisleep 1;

_newveh = createVehicle [_vehtype, [0,0,10000],[],0,""];
_newveh allowdamage false;
_newveh setDir _dir;
_newveh setPos _pos;
_newveh setVariable ["pos", _pos];
_newveh setVariable ["dir", _dir];
_newveh setVariable ["delay", _delay];
_newveh setVariable ["side", _side];
_newveh setVariable ["vehclass", _vehclass];
[_newveh, _side, _delay, _vehclass] spawn aas_fnc_initVehicle;

pV_hud_message = [["WGP_VehRespawned", _name], _side];
publicvariable "pV_hud_message";
if hasInterface then {pv_hud_message call wgp_fnc_hud_showMessage};
uisleep 2;
_newveh allowdamage true;