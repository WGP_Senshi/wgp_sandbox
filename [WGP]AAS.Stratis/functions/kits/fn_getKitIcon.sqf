_kitname = _this select 0;
_type = toLower (_this select 1);

private ["_currentkit", "_icon"];
_icon = "iconman";
_icon = switch (_kitname) do {
  case "Squadleader" : {"iconmanofficer"};
  case "FTLeader" : {"iconmanleader"};
  case "Medic" : {"iconmanmedic"};
  case "LAT" : {"iconmanat"};
  case "HAT" : {"iconmanexplosive"};
  case "AR" : {"iconmanmg"};
  case "MG" : {"iconmanmg"};
  case "Grenadier" : {"iconmanvirtual"};
  case "Sniper" : {"iconmanrecon"};
  case "Spotter" : {"iconmanrecon"};
  case "Engineer" : {"iconmanengineer"};
  default {"iconman"};
};
if (_type == "img") then {
    _icon = "A3\UI_F\data\map\VehicleIcons\" + _icon + "_ca.paa";
};
_icon