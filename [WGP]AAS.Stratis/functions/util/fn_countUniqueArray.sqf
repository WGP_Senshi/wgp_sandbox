/* ----------------------------------------------------------------------------
Function: wgp_fnc_countUniqueArray

Description:
    A function used to return the count of unique in an array.

Parameters:
    Array

Example:
    (begin example)
    _types = [0,0,1,1,1,1] call wgp_fnc_countUniqueArray
    _types2 = ["a","a","a","b","b","c"] call wgp_fnc_countUniqueArray
    (end)

Returns:
    Array element counts. For above examples:
		_types = [[0,2],[1,4]];
        _types2 = [["a",3],["b",2],["c",1]];

Author:
    Original CBA_fnc_getArrayElements by Rommel && sbsmac
	Modified by Senshi

---------------------------------------------------------------------------- */

private ["_array", "_return", "_countA", "_var", "_countB"];

_array =+ _this;
_return = [];
_countA = count _array;
while {_countA > 0} do {
     _var = _array select 0;
     _array = _array - [_var];
     _countB = count _array;
     _return = _return + [[_var, _countA - _countB]];
     _countA = _countB;
};
_return