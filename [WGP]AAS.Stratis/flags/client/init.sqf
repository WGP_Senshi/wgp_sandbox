if (!hasInterface) exitwith {};

WGP_AAS_sideloadouts = switch (side group player) do {
    case West: { loadouts_setup select 0};
    case East: { loadouts_setup select 1};
    case Resistance: { loadouts_setup select 2};
    default { loadouts_setup select 3};
};

("WGP_AAS_message" call BIS_fnc_rscLayer) cutRsc ["WGP_AAS_MESSAGE","PLAIN",1];
("WGP_AAS_TICKETS" call BIS_fnc_rscLayer) cutRsc ["WGP_AAS_TICKETS","PLAIN",1];
uiNameSpace getVariable "WGP_AAS_TICKETS" ctrlsetStructuredText (
	parseText (
    	str(p_AAS_tickets_west) + " <img size='1' color='#ffffff' image='" + team1_flag + "'/> "
			+ "<img size='1' color='#ffffff' image='" + team2_flag + "'/>" + str(p_AAS_tickets_east)
    )
);

{
   (_x select 0) setMarkerAlphaLocal 0; 
} foreach pV_spawnpoints;


execVM "flags\client\fn_showWeightInventory.sqf";
execVM "squadmarker\init.sqf";
_setup_eventhandlers = execVM "flags\client\setupEventhandler.sqf";
_markerinit = execVM "flags\client\refreshMarkers.sqf";
waitUntil {scriptdone _markerinit};
execVM "flags\client\mainbaseInit.sqf";
call compile preprocessFileLineNumbers "flags\client\dlg\hud_mapbuttons.sqf";


// player setVariable ["playerKitID", nil]; // player has to remember what kit he has. E.g. for kit customization.



waituntil {scriptDone _setup_eventhandlers};
systemchat ("Are you JIP? Time is " + str time);
if (time > 1) then {
	// This is a JIP client. Request a status update of ongoing mission.
	pV_RequestClientId = player;
	publicVariableServer "pV_RequestClientId";
	systemchat ("Yes, you are a JIP player.");
} else {
    systemchat ("No, you are not JIP.");
};
waituntil {!isNull finddisplay 46};
(finddisplay 46) displayAddEventHandler ["KeyDown", {
    if !(_this select 1 in actionkeys "TeamSwitch") exitwith {false};
    if !dialog then {
        execVM "squadmgr\SHOW.sqf";
    };
    true
    
}];

// Start as rifleman
this = player;
call compile preprocessFileLineNumbers ("barracks\loadouts\" + str (playerside) + "\Rifleman.sqf");
player setVariable ["playerKitID", 3]; // player has to remember what kit he has. E.g. for kit customization.
pV_playerkitid = [player, player getVariable "playerKitID"];
publicVariable "pv_playerkitid";
this = nil;