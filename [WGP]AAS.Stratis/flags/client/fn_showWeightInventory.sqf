player removeallEventHandlers "InventoryOpened";
_x = player addEventHandler ["InventoryOpened", {
    [] spawn {
	    waitUntil {!isnull (finddisplay 602)};
	    _x2 = player addEventHandler ["InventoryClosed", {
	        player removeEventHandler ["InventoryOpen", missionnamespace getVariable "temp_UI_invopen_handler"];
            player removeEventHandler ["InventoryClosed", missionnamespace getVariable "temp_UI_invclosed_handler"];
	        removeMissionEventHandler ["Draw3D", missionnamespace getVariable "temp_UI_redraw"];
	    }];
        _whileopen = addMissionEventHandler ["Draw3D", {
	    	(finddisplay 602 displayctrl 111) ctrlsetText (name player + " " + ([loadAbs player] call wgp_fnc_str_formatWeight));
        }];
		missionnamespace setVariable ["temp_UI_redraw", _whileopen];
		missionnamespace setVariable ["temp_UI_invclosed_handler", _x2];
	    
    };
}];
missionnamespace setVariable ["temp_UI_invopen_handler", _x];