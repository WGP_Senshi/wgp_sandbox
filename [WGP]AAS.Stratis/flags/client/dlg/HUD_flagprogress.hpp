class WGP_AAS_FLAG_PROGRESS
	{
    idd = -1;
    name = "WGP_AAS_FLAG_PROGRESS";
    duration = 999999;
	fadeIn = 1;
	fadeOut = 1;
    movingEnable = false;
    onLoad = "uiNameSpace setVariable ['AAS_Flag_ctrlProgressBar', (_this select 0) displayCtrl 1]; uiNamespace setVariable ['AAS_Flag_ctrlProgressBar_Title', (_this select 0) displayCtrl 2];";
    
    
    class controlsBackground
    {
        class WGP_AAS_FLAG_PROGRESSBAR : RscProgress
		{
			idc = 1;
	        moving = 0;
	        text = "";
	        sizeEx = "0.02";// * safezoneX / safezoneXAbs";
			// tooltip = " Unknown / Infinity";
			colorFrame[] = {0,0,0,1};
			colorBackground[] = {0.3, 0.3, 0.3, 0.3};
			colorBar[] = {1,1,1,1};
			texture = "";
			colorText[] = {1,1,1,1};
			lineSpacing = 0;
			access = 0;
			style = 2;
			size = 1;
			x = "safezoneX + 0.32";
			y = "safezoneY + 0.022";
			w = "0.3 * safezoneW";
			h = "0.03";
		};
        class WGP_AAS_FLAG_PROGRESSBAR_Title : RscText
		{
			idc = 2;
	        moving = 0;
	        text = "Test";
	        sizeEx = "0.025";// * safezoneX / safezoneXAbs";
			colorBackground[] = { 1, 1, 1, 0 };
			colorText[] = {1,1,1,1};
			lineSpacing = 0;
			style = 0x02;
			size = 1;
			x = "safezoneX + 0.32";
			y = "safezoneY + 0.022";
			w = "0.3 * safezoneW";
			h = "0.03";
		};
    };
    objects[] = {};
    controls[] = {};
    

};
class WGP_AAS_message
{
    idd = -1;
    name = "WGP_AAS_MESSAGE";
    duration = 999999;
	fadeIn = 1;
	fadeOut = 1;
    movingEnable = false;
    onLoad = "uiNameSpace setVariable ['WGP_AAS_MESSAGE', (_this select 0) displayCtrl 3];";
    
    
    class controlsBackground
    {
        class WGP_AAS_MESSAGE : RscStructuredText
		{
			idc = 3;
	        sizeEx = "0.02";
			size = "0.03";
			colorFrame[] = {0,0,0,1};
			colorBackground[] = {0.3, 0.3, 0.3, 0.3};
			colorText[] = {1,1,1,1};
			lineSpacing = 1;
			access = 0;
			style = 2;
			x = "safezoneX + 0.32";
			y = "safezoneY + 0.022 + 0.03";
			w = "0.3 * safezoneW";
			h = "0.13";
		};
    };
    objects[] = {};
    controls[] = {};
    

};
class WGP_AAS_tickets
{
    idd = -1;
    name = "WGP_AAS_TICKETS";
    duration = 999999;
	fadeIn = 1;
	fadeOut = 1;
    movingEnable = false;
    onLoad = "uiNameSpace setVariable ['WGP_AAS_TICKETS', (_this select 0) displayCtrl 4];";
    
    
    class controlsBackground
    {
        class WGP_AAS_MESSAGE : RscStructuredText
		{
			idc = 4;
	        sizeEx = "0.05";
			size = "0.05";
			colorFrame[] = {0,0,0,1};
			colorBackground[] = {0.3, 0.3, 0.3, 0};
			colorText[] = {1,1,1,1};
			lineSpacing = 1;
			access = 0;
			style = 2;
			x = "safezoneX + 0.55 * safezoneW";
			y = "safezoneY + 0.022";
			w = "0.3 * safezoneW";
			h = "0.06";
		};
    };
    objects[] = {};
    controls[] = {};
	    
	
};