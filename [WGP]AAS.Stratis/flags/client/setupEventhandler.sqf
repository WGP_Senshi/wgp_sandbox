// Initialize public variables and event listeners

if !hasInterface exitwith {}; // Only client has to do this

_EH_playerKilled_first = player addEventHandler ["Killed", {
    if (true) exitWith { // Catch first spawn by corpse detection. If there is no corpse, you must be new!
    	player removeAllEventHandlers "killed";
    	_EH_playerKilled = player addEventHandler ["Killed", {
	    	_this spawn {
                private "_killer";
			    _killer = _this select 1;
			    sleep 1;
			    if (!(player getvariable ["BIS_revive_incapacitated", false])) then {
				    	pv_addTickets = [playerside, -1];
				    	publicVariableServer "pv_addTickets";
			    };
	    	};
        }];
        
	};
}];

_EH_playerRespawn = player addEventHandler ["Respawn", {
    _this spawn {
	    if (player getVariable ["BIS_revive_incapacitated", false]) exitWith {};
	    if (!isNil {player getVariable "playerKitID"}) then {
            private "_scriptfilename";
	    	_scriptFilename = WGP_AAS_sideloadouts select (player getVariable "playerKitID") select 1;
			systemchat ("INFO: Respawn as: " + (WGP_AAS_sideloadouts select (player getVariable "playerKitID") select 0));
			this = player;
	        try {
				call compile preprocessFileLineNumbers ("barracks\loadouts\" + str (playerside) + "\" + _scriptFilename + ".sqf");
	        } catch {
				call compile preprocessFileLineNumbers ("barracks\loadouts\" + str (playerside) + "\Rifleman.sqf");
	        };
	        this = nil;
	    };
    };
}];


"pV_AAS_refreshMarkers" addPublicVariableEventHandler {
    execVM "flags\client\refreshMarkers.sqf"
};

"pV_hud_message" addPublicVariableEventHandler {
    pv_hud_message call wgp_fnc_hud_showMessage;
};
"pV_updateflag" addPublicVariableEventHandler {
    private ["_base", "_trigger", "_name"];
    _base = pv_bases select (_this select 1 select 0);
    _trigger = _base select 0;
    _name = _trigger getVariable "name";
    _trigger setVariable ["Side", _this select 1 select 1];
    _trigger setVariable ["sideattack", _this select 1 select 2];
    _trigger setVariable ["CapPoints", _this select 1 select 3];
    
    execVM "flags\client\refreshMarkers.sqf";
};

"pV_setTickets" addPublicVariableEventHandler {
    private ["_side", "_tickets"];
    _side = _this select 1 select 0;
    _tickets = _this select 1 select 1;
    if (_side == West) then {
    	p_AAS_Tickets_West = _tickets;
    } else {
        p_AAS_Tickets_East = _tickets;
    };
    
    _side execVM "flags\client\refreshTickets.sqf";
};
"pV_playRadio" addPublicVariableEventHandler {
    _this select 1 call wgp_fnc_misc_playRadio;
};
"pV_missionEnd" addPublicVariableEventHandler {
    
    _victor = _this select 1;
    if (playerside == _victor) then {
        ["victory", true] call BIS_fnc_endMission;
    } else {
        ["defeat", false] call BIS_fnc_endMission;
    }
};