"pV_changeGroupID" addPublicVariableEventHandler {
    systemchat ("JIP SYNC: GroupID changed to " + str (_this select 1)) ;
    (_this select 1) spawn wgp_fnc_setGroupID
};

"pV_changeRank" addPublicVariableEventHandler {
    (_this select 1) spawn wgp_fnc_setRank 
};

"pv_playerkitid" addPublicVariableEventHandler {
    private ["_unit", "_kitID"];
    _unit = _this select 1 select 0;
    _kitID = _this select 1 select 1;
    _unit setVariable ["playerkitid", _kitID]; 
    
    
    if (!isNull (finddisplay WGP_IDD_BARRACK)) then {
		call compile preprocessFileLineNumbers "barracks\updateGUI.sqf";
    }
    
};