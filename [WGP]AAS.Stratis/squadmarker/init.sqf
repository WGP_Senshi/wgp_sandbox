if (!hasInterface) exitWith {};

waitUntil {!isNull player and isPlayer player };
sleep 1;
_eh = ((findDisplay 12) displayCtrl 51) ctrlAddEventHandler ["Draw", '
    if (!visibleMap and !visibleGPS) exitWith {}; // Only go through all the hazzle if the map is actually open
    
	private["_lastMarkerCount","_colors", "_sides", "_colorsRGB", "_colorWest", "_colorEast", "_colorGUER", "_colorNeutral", "_map"];
	 private["_units", "_color", "_colorRGB", "_side"];
    
	_map = _this select 0;
	_lastMarkerCount = 0;
	_colorGUER = getarray (configfile >> "CfgMarkerColors" >> "ColorGUER" >> "color");
	{ _colorGUER set [_foreachindex, call compile _x] } foreach _colorGUER;
	_sides = [West, East, Resistance, Civilian];
	_colorsRGB = [team1_color, team2_color, _colorGUER, teamneutral_color];
	_colors = ["ColorBLUFOR", "ColorEAST", "ColorIndependent", "ColorCivilian"];

	_units = [playableUnits, {isPlayer _x && (side group _x ==  (playerside) or side group _x == civilian)}] call BIS_fnc_conditionalSelect; // Only register player units on own side. Civs are added, because incapacitated players are civilians (=ignored by enemy AI).
    if (count _units != _lastMarkerCount) then {
		private ["_i", "_markerName"];
		// Delete markers
		for "_i" from 0 to (_lastMarkerCount - 1) do {
			deleteMarkerLocal format["WGP_sqdmarker%1", _i];
		};
		
		//Recreate markers
		_lastMarkerCount = count _units;
		for "_i" from 0 to (_lastMarkerCount - 1) do {
			createMarkerLocal [format["WGP_sqdmarker%1", _i], [0,0,0]];
		};
	};
    
	// Refresh markers
	for "_i" from 0 to _lastMarkerCount-1 do {
		private["_unit"];
		_unit = _units select _i;
        if  (side group _unit == playerside) then {
            _rank = rank _unit;
			_markerName = format["WGP_sqdmarker%1", _i];
            missionNameSpace setVariable [format ["WGP_sqdmarker%1", _i], _unit];
			_markerName setMarkerPosLocal (getPos _unit);
            if (_sides find (side group _unit) != -1) then {
		    	_color = _colors select (_sides find side group _unit);
		        _colorRGB = _colorsRGB select (_sides find side group _unit);
		    } else {
		        _color = "ColorBlack";
		        _colorRGB = _colorsRGB select (count _colorsRGB -1);
		    };
            
            
            
            
            _markerUnit = if (count (ctrlMapMouseOver _map) > 1) then {
                	//systemchat str(ctrlMapMouseOver _map);
	                if ((ctrlMapMouseOver _map) select 0 == "marker" and ["WGP_sqdmarker", (ctrlMapMouseOver _map) select 1] call wgp_fnc_findInString) then {
                    	if !isNil {missionnamespace getVariable ((ctrlMapMouseOver _map) select 1)} then {
	                		missionnamespace getVariable ((ctrlMapMouseOver _map) select 1);
	                	};
                    };
        	} else {nil};
            _string = "";
            if (_markerUnit == _unit) then {
                if (vehicle _markerUnit != _markerUnit) then {
                    _control = finddisplay 12 displayctrl 999;
                    if (isNull (finddisplay 12 displayctrl 999)) then {
		            	_control = findDisplay 12 ctrlCreate [ "RscStructuredText", 999 ];
                    } else {
                        _control = finddisplay 12 displayctrl 999;
                    };
					
                    _control ctrlSetTextColor _colorRGB;
					// _pos2D = worldToScreen ( ( getPosVisual _unit ) vectorAdd [ 2 / ctrlMapScale _map , 0, 0 ] );
                    _pos2D = _map ctrlMapWorldToScreen ( ( getPosVisual _unit ) vectorAdd [ 200 * (ctrlMapscale _map max 0.05), 100 * (ctrlMapscale _map max 0), 0 ] );
                    				
					if ( count _pos2D > 0 ) then {
						
						_control ctrlSetPosition [
							(_pos2D select 0),
							(_pos2D select 1),
							1,
							1
						];
				
						_control ctrlSetFade 0;
						_control ctrlCommit 0;
					}else{
						_control ctrlSetFade 1;
						_control ctrlCommit 0;
					};
                    {
                        if (_foreachindex > 0) then {
                            _string = _string + "<br/>";
                        };
            			_string = _string + name _x;
			        	if !isNil {group _markerUnit getVariable "CustomGroup"} then {
			        		_string = _string + " " + groupID group _markerUnit
			    		};
                        
                    } foreach crew vehicle _markerUnit;
					_control ctrlSetStructuredText parseText _string;
                } else {
        			_string = name _markerUnit;
	                if (!isNull (finddisplay 12 displayCtrl 999)) then {
	                	ctrlDelete ((findDisplay 12 displayCtrl 999));
	                };
                };
            };
            if (isNil "_markerUnit") then {
                if (!isNull (finddisplay 12 displayCtrl 999)) then {
	            	ctrlDelete ((findDisplay 12 displayCtrl 999));
                };
            };
            
            // If unit is INCAPACITATED            
			if (_unit getVariable ["BIS_revive_incapacitated", false]) then {
                _map drawIcon [
					"a3\ui_f\data\IGUI\Cfg\Actions\heal_ca.paa",
					[0.8,0,0,1],
					getPos _unit,
                    24,
                    24,
			        //0.5/ctrlMapScale (_this select 0),
			        //0.5/ctrlMapScale (_this select 0),
					0, //getDir _unit,
                    _string,
					1,
					0.06,
					"PuristaMedium",
					"right"
				];
			} else { //If ALIVE
                // If IN VEHICLE
                if (vehicle _unit != _unit ) then {
                    _map drawIcon [
				        getText (configFile/"CfgVehicles"/typeOf (vehicle _unit)/"Icon"),
				        _colorRGB,
				        getPos (vehicle _unit),
                        32,
                        32,
				        //0.5/ctrlMapScale (_this select 0),
				        //0.5/ctrlMapScale (_this select 0),
				        direction (vehicle _unit),
                        "",
						1,
						0.06,
						"PuristaMedium",
						"right"
				    ];
                } else {
            	// if ON FOOT
                    private "_icon";
                    _icon = "iconman";
                    if (!isNil {loadouts select (_unit getvariable "playerkitid")}) then {
                        private "_currentkit";
                        switch (side group player) do {
						    case West: { loadouts = loadouts_setup select 0};
						    case East: { loadouts = loadouts_setup select 1};
						    case Resistance: { loadouts = loadouts_setup select 2};
						    default { loadouts = loadouts_setup select 3};
						};
				        _currentkit = loadouts select (_unit getvariable "playerkitid");
                        _icon = [_currentkit select 1, "map"] call wgp_fnc_getKitIcon;
				    };
                    // if (_unit == leader group _unit and !isNil {group _unit getVariable "CustomGroup"}) then { _icon = "iconmanofficer"};
                 	_map drawIcon [
				        _icon,
				        _colorRGB,
				        getPos _unit,
                        24,
                        24,
				        //0.5/ctrlMapScale (_this select 0),
				        //0.5/ctrlMapScale (_this select 0),
				        direction _unit,
                        _string,
						1,
						0.06,
						"PuristaMedium",
						"right"
				    ];
                };
            };
            
            // If GROUP LEADER
			if (_unit == leader group _unit and !isNil {group _unit getVariable "CustomGroup"}) then {
                _map drawIcon [
			        "#(argb,8,8,3)color(0,0,0,0)",
			        _colorRGB,
			        //[visiblePosition _unit select 0, (visiblePosition _unit select 1) + 12 * 0.5 / ctrlMapscale (_this select 0), visiblePosition _unit select 2],
                    [getPos _unit select 0, (getPos _unit select 1) + (6 max 256 * ctrlMapScale _map), getPos _unit select 2],
                    32,
                    32,
			        //0.5/ctrlMapScale (_this select 0),
			        //0.5/ctrlMapScale (_this select 0),
			        0,
                    groupID group _unit,
					1,
					0.1,
					"PuristaBold",
					"center"
			    ];
			};
		};
	};
'];
