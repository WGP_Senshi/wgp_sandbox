	sounds[] = {};
	class USTownLost1
	{
		name = "";
		sound[] = {"rsc\sounds\us\wehavelostcontrolof.ogg", 10, 1};
		title = "We have lost control of an outpost.";
	};
    class USTownLost2
	{
		name = "";
		sound[] = {"rsc\sounds\us\wehavelostcontrolof2.ogg", 10, 1};
		title = "We have lost control of an outpost.";
	};
    class USTownLost3
	{
		name = "";
		sound[] = {"rsc\sounds\us\wehavelostcontrolof3.ogg", 10, 1};
		title = "We have lost control of an outpost.";
	};
    class USTownLost4
	{
		name = "";
		sound[] = {"rsc\sounds\us\WeAreTakingHeavyCasualities.ogg", 10, 1};
		title = "We have lost control of an outpost.";
	};
	class USTownCaptured1
	{
		name = "";
		sound[] = {"rsc\sounds\us\wenowhavecontrolover.ogg", 10, 1};
		title = "We have taken control of an outpost.";
	};
    class USTownCaptured2
	{
		name = "";
		sound[] = {"rsc\sounds\us\wenowhavecontrolover2.ogg", 10, 1};
		title = "We have taken control of an outpost.";
	};
    class USTownCaptured3
	{
		name = "";
		sound[] = {"rsc\sounds\us\wenowhavecontrolover3.ogg", 10, 1};
		title = "We have taken control of an outpost.";
	};
    class USTownCaptured4
	{
		name = "";
		sound[] = {"rsc\sounds\us\wenowhavecontrolover4.ogg", 10, 1};
		title = "We have taken control of an outpost.";
	};
	class UKTownLost1
	{
		name = "";
		sound[] = {"rsc\sounds\UK\WeHaveLostControlOf.ogg", 10, 1};
		title = "We have lost control of an outpost.";
	};
    class UKTownLost2
	{
		name = "";
		sound[] = {"rsc\sounds\UK\WeHaveLostControlOf2.ogg", 10, 1};
		title = "We have lost control of an outpost.";
	};
    class UKTownLost3
	{
		name = "";
		sound[] = {"rsc\sounds\UK\WeHaveLostControlOf3.ogg", 10, 1};
		title = "We have lost control of an outpost.";
	};
	class UKTownCaptured1
	{
		name = "";
		sound[] = {"rsc\sounds\UK\WeNowHaveControlOver.ogg", 10, 1};
		title = "We have taken control of an outpost.";
	};
	class UKTownCaptured2
	{
		name = "";
		sound[] = {"rsc\sounds\UK\WeNowHaveControlOver2.ogg", 10, 1};
		title = "We have taken control of an outpost.";
	};
	class UKTownCaptured3
	{
		name = "";
		sound[] = {"rsc\sounds\UK\WeNowHaveControlOver3.ogg", 10, 1};
		title = "We have taken control of an outpost.";
	};
	class USTicketBleedStart1
	{
		name = "";
		sound[] = {"rsc\sounds\US\auto_rules_ticketbleedstart.ogg", 10, 1};
		title = "We are losing control of the fight.";
	};
	class USTicketBleedStart2
	{
		name = "";
		sound[] = {"rsc\sounds\US\auto_rules_ticketbleedstart_alt.ogg", 10, 1};
		title = "We are losing control of the fight.";
	};
	class USTicketBleedEnd1
	{
		name = "";
		sound[] = {"rsc\sounds\US\auto_rules_ticketbleedend.ogg", 10, 1};
		title = "We have stabilized our positions.";
	};
	class USTicketBleedEnd2
	{
		name = "";
		sound[] = {"rsc\sounds\US\auto_rules_ticketbleedend_alt.ogg", 10, 1};
		title = "We have stabilized our positions.";
	};