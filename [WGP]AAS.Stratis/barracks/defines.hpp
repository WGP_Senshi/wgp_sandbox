#define GRID_ABS_W		1
#define GRID_ABS_H		1
#define GRID_TITLE_H 	0.05
#define GRID_COUNT_W 	40
#define GRID_COUNT_H 	20

/* Automatisch zentriert berechnen */
#define GRID_W			(GRID_ABS_W / GRID_COUNT_W)
#define GRID_H			(GRID_ABS_H / GRID_COUNT_H)
#define GRID_ABS_X		(GRID_ABS_W / 2 + GRID_ABS_W / 2)
#define GRID_ABS_X_RIGHT (GRID_ABS_W / 4 + GRID_ABS_W)
#define GRID_ABS_Y		(0)



/* IDs 850-899 */
#define WGP_IDD_BARRACK					850
#define WGP_IDC_BARRACK_Title 			851
#define WGP_IDC_BARRACK_SubTitle 		852
#define WGP_IDC_BARRACK_List 			853
#define WGP_IDC_BARRACK_ButtonOK			854
#define WGP_IDC_BARRACK_ButtonCancel 	855

#define WGP_IDD_BARRACK_Weapons			856
#define WGP_IDC_BARRACK_Weapons_List 	857

#define WGP_IDC_BARRACK_InfoText 			858