#include "defines.hpp";
_kitcounts = player call wgp_fnc_getKitCounts;
_squadsize = count units group player;
_teamsize = {side _x == side group player} count allplayers;

{
    	_maxpersquad = _x select 2;
        _squadplayersperkit = _x select 3;
        _teamplayersperkit = _x select 4;
        _kitinsquad = _kitcounts select 1 select _foreachindex;
        _kitinteam = _kitcounts select 0 select _foreachindex;
        _available = false;
        _check_isinsquad = !isnil {group player getvariable "customgroup"};
        _check_maxpersquad = (_kitinsquad < _maxpersquad);
        _check_squadplayersperkit = _squadsize >= (_kitinsquad +1) * _squadplayersperkit;
        _check_teamplayersperkit = _teamsize >= (_kitinteam + 1) * _teamplayersperkit;
        /*if (_foreachindex == 5) then {
			systemchat str(_kitcounts);
			systemchat str(_squadsize);
			systemchat str(_teamsize);
        	systemchat ("MAX SQUAD " +  str(_maxpersquad) + str(_check_maxpersquad));
            systemchat ("MAX _squadplayersperkit " +  str(_squadplayersperkit) + str(_check_squadplayersperkit));
            systemchat ("MAX _teamplayersperkit " +  str(_teamplayersperkit) + str(_check_teamplayersperkit));
            systemchat ("MAX _kitinsquad " +  str(_kitinsquad));
            systemchat ("MAX _kitinteam " +  str(_kitinteam));
            systemchat ("MAX _check_isinsquad " +  str(_check_isinsquad));
        };*/
        _availablestr = "";
        if (!_check_isinsquad) then {
            _availablestr = _availablestr + "You must join a squad (press 'U') to be able to request a kit!<br/>";
        };
        if (!_check_maxpersquad) then {
            _availablestr = _availablestr + "Each squad can only have  " + str(_maxpersquad) + " kits of this type.<br/>";
        };
        if (!_check_squadplayersperkit) then {
            _availablestr = _availablestr + "You need " + str(abs (_squadsize - (_kitinsquad +1) * _squadplayersperkit)) + " more players in your squad to request this kit.<br/>";
        };
        if (!_check_teamplayersperkit) then {
            _availablestr = _availablestr + "You need " + str(abs (_teamsize - (_kitinteam +1) * _teamplayersperkit)) + " more players on your side to request this kit.<br/>";
        };
        if (_check_isinsquad and _check_maxpersquad and _check_squadplayersperkit and _check_teamplayersperkit) then {
            _available = true;
            lbSetColor [WGP_IDC_BARRACK_List,_foreachindex, [0,0.7,0,1]];
            lbSetData [WGP_IDC_BARRACK_List, _foreachindex, "You can request this kit!"];
            lbSetValue [WGP_IDC_BARRACK_List, _foreachindex, 1];
        } else {
         	lbSetColor [WGP_IDC_BARRACK_List,_foreachindex, [0.7,0,0,1]];
            lbSetData [WGP_IDC_BARRACK_List, _foreachindex, _availablestr];
            lbSetValue [WGP_IDC_BARRACK_List, _foreachindex, 0];
        };
	    if (_foreachindex == player getVariable "playerkitid") then {
            lbSetColor [WGP_IDC_BARRACK_List,_foreachindex, [0.7,0.6,0,1]];
            lbSetData [WGP_IDC_BARRACK_List, _foreachindex, "You already have this kit!"];
	    };
         lbSetPicture [WGP_IDC_BARRACK_List, _foreachindex, [_x select 1, "img"] call wgp_fnc_getKitIcon ];
} foreach WGP_AAS_sideloadouts;