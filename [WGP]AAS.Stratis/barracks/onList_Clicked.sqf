private["_scriptFilename", "_result"];
disableSerialization;
#include "defines.hpp"

if (WGPBarrack_Selection == lbCurSel WGP_IDC_BARRACK_List) exitWith{}; // Prevent loading the current entry multiple times 

WGPBarrack_Selection = lbCurSel WGP_IDC_BARRACK_List;
finddisplay WGP_IDD_BARRACK displayctrl WGP_IDC_BARRACK_InfoText ctrlSetStructuredText parseText (lbData [WGP_IDC_BARRACK_List, WGPBarrack_Selection]);

_ctrl_weapons = finddisplay WGP_IDD_BARRACK displayctrl WGP_IDC_BARRACK_Weapons_List;
lbClear _ctrl_weapons;

WGP_fnc_findClass = {
    if isText(configFile >> "CfgWeapons" >> _this >> "displayName") then {
        configFile >> "CfgWeapons" >> _this
    } else {
        if isText(configFile >> "CfgMagazines" >> _this >> "displayName") then {
	        configFile >> "CfgMagazines" >> _this
	    } else {
	        if isText(configFile >> "CfgVehicles" >> _this >> "displayName") then {
		        configFile >> "CfgVehicles" >> _this
	        } else {
		        if isText(configFile >> "CfgGlasses" >> _this >> "displayName") then {
			        configFile >> "CfgGlasses" >> _this
                }
            }
        }
    };
};

_getDisplayName = {getText((_this call WGP_fnc_findClass) >> "displayName")};
_getPic =  {getText((_this call WGP_fnc_findClass) >> "picture")};


// Item counting

_scriptFilename = (WGP_AAS_sideloadouts select WGPBarrack_Selection) select 1;
call compile preprocessFileLineNumbers ("barracks\loadouts\" + str (playerside) + "\" + _scriptFilename + ".sqf");


_hmd = hmd this; // Head-Mounted Display = NVG
_headgear = headgear this;
_binocular = binocular this;
_uniform = uniform this;
_vest = vest this;
_backpack = backpack this;
_goggles = goggles this;

_primaryWeapon = primaryWeapon this;
// _primaryWeaponMagazine = primaryWeaponMagazine this;
_secondaryWeapon = secondaryWeapon this;
// _scondaryWeaponMagazine = secondaryWeaponMagazine this;
_handgunWeapon = handgunWeapon this;
// _handgunMagazine = handgunMagazine this;

_weapons = weapons this;
_magazines = magazines this;
_items = items this;
_assigneditems = assignedItems this;
_vestitems = vestitems this;
_uniformitems = uniformitems this;
_backpackitems = backpackitems this;
_handgunWeaponItems = handgunitems this;
_primaryWeaponItems = primaryWeaponItems this;
_secondaryWeaponItems = secondaryWeaponItems this;

_items_flat = _magazines + _items + _assigneditems;
_items_unique = _items_flat call wgp_fnc_uniquearray;

if (_primaryWeapon != "") then {
	_ctrl_weapons lbAdd (_primaryWeapon call _getDisplayName);
	_ctrl_weapons lbSetPicture[(lbSize _ctrl_weapons) -1, _primaryWeapon call _getPic];
	{
	    if (_x != "") then {
		    _ctrl_weapons lbAdd (_x call _getDisplayName);
			_ctrl_weapons lbSetPicture[(lbSize _ctrl_weapons) -1, _x call _getPic];
	    };
	} foreach _primaryWeaponItems;
};
if (_secondaryWeapon != "") then {
	_ctrl_weapons lbAdd (_secondaryWeapon call _getDisplayName);
	_ctrl_weapons lbSetPicture[(lbSize _ctrl_weapons) -1, _secondaryWeapon call _getPic];
	{
	    if (_x != "") then {
		    _ctrl_weapons lbAdd (_x call _getDisplayName);
			_ctrl_weapons lbSetPicture[(lbSize _ctrl_weapons) -1, _x call _getPic];
	    };
	} foreach _secondaryWeaponItems;
};
if (_handgunWeapon != "") then {
	_ctrl_weapons lbAdd (_handgunWeapon call _getDisplayName);
	_ctrl_weapons lbSetPicture [(lbSize _ctrl_weapons) -1, _handgunWeapon call _getPic];
	{
	    if (_x != "") then {
		    _ctrl_weapons lbAdd (_x call _getDisplayName);
			_ctrl_weapons lbSetPicture [(lbSize _ctrl_weapons) -1, _x call _getPic];
	    };
	} foreach _handgunWeaponItems;
};
{
    _item = _x;
    _count = count([_items_flat, {_x == _item}] call BIS_fnc_conditionalSelect);
    if (_item != "") then {
	    if (_count > 1) then {
		    _ctrl_weapons lbAdd (format ["%1x %2 ",_count, _item call _getDisplayName]);
		    _ctrl_weapons lbSetPicture[(lbSize _ctrl_weapons) -1, _item call _getPic];
	    } else {
	        _ctrl_weapons lbAdd (_item call _getDisplayName);
	        _ctrl_weapons lbSetPicture[(lbSize _ctrl_weapons) -1, _item call _getPic];
	    };
    };
} foreach _items_unique;

if (_goggles != "") then {
	_ctrl_weapons lbAdd (_goggles call  _getDisplayName);
    _ctrl_weapons lbSetPicture[(lbSize _ctrl_weapons) -1, _goggles call _getPic];
};
if (_headgear != "") then {
	_ctrl_weapons lbAdd (_headgear call  _getDisplayName);
    _ctrl_weapons lbSetPicture[(lbSize _ctrl_weapons) -1, _headgear call _getPic];
};
if (_uniform != "") then {
	_ctrl_weapons lbAdd (_uniform call  _getDisplayName);
    _ctrl_weapons lbSetPicture[(lbSize _ctrl_weapons) -1, _uniform call _getPic];
};
if (_vest != "") then {
	_ctrl_weapons lbAdd (_vest call  _getDisplayName);
    _ctrl_weapons lbSetPicture[(lbSize _ctrl_weapons) -1, _vest call _getPic];
};
if (_backpack != "") then {
	_ctrl_weapons lbAdd (_backpack call  _getDisplayName);
    _ctrl_weapons lbSetPicture[(lbSize _ctrl_weapons) -1, _backpack call _getPic];
};