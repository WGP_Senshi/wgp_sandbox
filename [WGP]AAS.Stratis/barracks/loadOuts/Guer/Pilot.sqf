comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_I_pilotCoveralls";
this addItemToUniform "FirstAidKit";
this addItemToUniform "SmokeShellRed";
for "_i" from 1 to 3 do {this addItemToUniform "30Rnd_9x21_Mag";};
this addBackpack "B_Parachute";
this addHeadgear "H_PilotHelmetHeli_I";

comment "Add weapons";
this addWeapon "hgun_PDW2000_F";
this addPrimaryWeaponItem "optic_ACO_grn_smg";

this addItemToUniform "30Rnd_9x21_Mag";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemRadio";
this linkItem "ItemGPS";
this linkItem "NVGoggles_INDEP";
