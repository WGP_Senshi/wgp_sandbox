comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_I_CombatUniform";
this addItemToUniform "FirstAidKit";
this addItemToUniform "HandGrenade";
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addVest "V_PlateCarrierIA1_dgtl";
for "_i" from 1 to 6 do {this addItemToVest "30Rnd_556x45_Stanag";};
this addBackpack "B_Carryall_oli";
this addItemToBackpack "ToolKit";
this addItemToBackpack "SatchelCharge_Remote_Mag";
this addItemToBackpack "ATMine_Range_Mag";
for "_i" from 1 to 3 do {this addItemToBackpack "DemoCharge_Remote_Mag";};
this addHeadgear "H_HelmetIA";

comment "Add weapons";
this addWeapon "arifle_Mk20C_F";
this addPrimaryWeaponItem "acc_pointer_IR";
this addPrimaryWeaponItem "optic_Hamr";

this addItemToVest "30Rnd_556x45_Stanag";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemRadio";
this linkItem "NVGoggles";