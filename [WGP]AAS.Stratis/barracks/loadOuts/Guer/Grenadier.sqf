comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_I_CombatUniform";
this addItemToUniform "FirstAidKit";
for "_i" from 1 to 2 do {this addItemToUniform "HandGrenade";};
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addVest "V_PlateCarrierIA1_dgtl";
for "_i" from 1 to 8 do {this addItemToVest "30Rnd_556x45_Stanag";};
for "_i" from 1 to 5 do {this addItemToVest "1Rnd_Smoke_Grenade_shell";};
for "_i" from 1 to 9 do {this addItemToVest "1Rnd_HE_Grenade_shell";};
this addHeadgear "H_HelmetIA";

comment "Add weapons";
this addWeapon "arifle_Mk20_GL_F";
this addPrimaryWeaponItem "acc_pointer_IR";
this addPrimaryWeaponItem "optic_Hamr";

this addItemToVest "30Rnd_556x45_Stanag";
for "_i" from 1 to 5 do {this addItemToVest "1Rnd_HE_Grenade_shell";};

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemRadio";
this linkItem "NVGoggles_INDEP";