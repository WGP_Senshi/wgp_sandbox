comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_B_CombatUniform_mcam_vest";
this addItemToUniform "FirstAidKit";
this addItemToUniform "HandGrenade";
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addItemToUniform "SmokeShellBlue";
this addVest "V_BandollierB_rgr";
for "_i" from 1 to 6 do {this addItemToVest "30Rnd_65x39_caseless_mag";};
this addHeadgear "H_HelmetCrew_B";

comment "Add weapons";
this addWeapon "arifle_MXC_F";
this addPrimaryWeaponItem "acc_pointer_IR";
this addPrimaryWeaponItem "optic_Aco";
this addWeapon "Binocular";

this addItemToVest "30Rnd_65x39_caseless_mag";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemRadio";
this linkItem "ItemGPS";
this linkItem "NVGoggles";