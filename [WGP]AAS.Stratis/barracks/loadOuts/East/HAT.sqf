comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_O_CombatUniform_ocamo";
this addItemToUniform "FirstAidKit";
this addItemToUniform "HandGrenade";
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addVest "V_Chestrig_khk";
for "_i" from 1 to 6 do {this addItemToVest "30Rnd_65x39_caseless_green";};
this addBackpack "B_AssaultPack_ocamo";
this addItemToBackpack "Titan_AT";
this addHeadgear "H_HelmetO_ocamo";

comment "Add weapons";
this addWeapon "arifle_Katiba_C_F";
this addPrimaryWeaponItem "acc_pointer_IR";
this addPrimaryWeaponItem "optic_Arco";
this addWeapon "launch_O_Titan_short_F";

this addItemToVest "30Rnd_65x39_caseless_green";
this addItemToBackpack "Titan_AT";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemRadio";
this linkItem "NVGoggles_OPFOR";
