comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_O_SpecopsUniform_ocamo";
this addItemToUniform "FirstAidKit";
this addItemToUniform "HandGrenade";
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addItemToUniform "SmokeShellRed";
this addVest "V_BandollierB_khk";
for "_i" from 1 to 6 do {this addItemToVest "30Rnd_65x39_caseless_green";};
this addHeadgear "H_HelmetCrew_O";

comment "Add weapons";
this addWeapon "arifle_Katiba_C_F";
this addPrimaryWeaponItem "acc_pointer_IR";
this addPrimaryWeaponItem "optic_ACO_grn";
this addWeapon "Binocular";

this addItemToVest "30Rnd_65x39_caseless_green";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemRadio";
this linkItem "ItemGPS";
this linkItem "NVGoggles_OPFOR";
