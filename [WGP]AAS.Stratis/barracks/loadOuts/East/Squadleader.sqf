comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_O_CombatUniform_ocamo";
this addItemToUniform "FirstAidKit";
for "_i" from 1 to 2 do {this addItemToUniform "HandGrenade";};
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addItemToUniform "SmokeShellRed";
this addVest "V_TacVest_khk";
for "_i" from 1 to 6 do {this addItemToVest "30Rnd_65x39_caseless_green";};
for "_i" from 1 to 2 do {this addItemToVest "30Rnd_65x39_caseless_green_mag_Tracer";};
for "_i" from 1 to 2 do {this addItemToVest "16Rnd_9x21_Mag";};
this addHeadgear "H_HelmetLeaderO_ocamo";

comment "Add weapons";
this addWeapon "arifle_Katiba_F";
this addPrimaryWeaponItem "acc_pointer_IR";
this addPrimaryWeaponItem "optic_Arco";
this addWeapon "hgun_Rook40_F";
this addWeapon "Rangefinder";

this addItemToVest "30Rnd_65x39_caseless_green";
this addItemToVest "16Rnd_9x21_Mag";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemRadio";
this linkItem "ItemGPS";
this linkItem "NVGoggles_OPFOR";
