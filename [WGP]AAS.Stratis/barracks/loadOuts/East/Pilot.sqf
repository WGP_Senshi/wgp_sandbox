comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_O_PilotCoveralls";
this addItemToUniform "FirstAidKit";
this addItemToUniform "SmokeShellRed";
for "_i" from 1 to 4 do {this addItemToUniform "30Rnd_9x21_Mag";};
this addBackpack "B_Parachute";
this addHeadgear "H_PilotHelmetHeli_O";

comment "Add weapons";
this addWeapon "SMG_02_F";
this addPrimaryWeaponItem "acc_pointer_IR";
this addPrimaryWeaponItem "optic_ACO_grn_smg";

this addItemToUniform "30Rnd_9x21_Mag";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemRadio";
this linkItem "ItemGPS";
this linkItem "NVGoggles_OPFOR";