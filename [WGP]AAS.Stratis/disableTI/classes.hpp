class DISABLETI 
	{
		idd = -1;
		name = "DISABLETI";
		duration = 9999999;
		movingEnable = false;
		
		controlsBackground[] = 
		{
			DISABLETI_Background
		};
		objects[] = {};
		controls[] =
		{
		};
		
		class DISABLETI_Background: IGUIBack
		{
			idc = -1;
			x = safeZoneXAbs;
			y = safeZoneY;
			w = safeZoneH;
			h = safeZoneWAbs;
			colorBackground[] = {0,0,0,1}; //yellow background
			colorText[] = {0,0,1,1}; //blue text
			text = "Thermals are DISABLED!";
		};
	}