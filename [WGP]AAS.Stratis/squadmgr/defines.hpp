#define GRID_ABS_W		1
#define GRID_ABS_H		1
#define GRID_TITLE_H 	0.05
#define GRID_COUNT_W 	40
#define GRID_COUNT_H 	20

/* Automatisch zentriert berechnen */
#define GRID_W			(GRID_ABS_W / GRID_COUNT_W)
#define GRID_H			(GRID_ABS_H / GRID_COUNT_H)
#define GRID_ABS_X		0
#define GRID_ABS_Y		0



/* IDs 950-1000 */
#define WGP_IDD_SQUADMGR					950
#define WGP_IDC_SQUADMGR_Title 			951
#define WGP_IDC_SQUADMGR_SubTitle 		952
#define WGP_IDC_SQUADMGR_List 			953
#define WGP_IDC_SQUADMGR_ButtonCreate 	954
#define WGP_IDC_SQUADMGR_ButtonJoin	 	955
#define WGP_IDC_SQUADMGR_ButtonLeave	 	956
#define WGP_IDC_SQUADMGR_ButtonCancel	 	957
#define WGP_IDC_SQUADMGR_EditName		 	958

#define WGP_IDC_SQUADMGR_Members_List 	960
#define WGP_IDC_SQUADMGR_Members_SubTitle 961