#include "defines.hpp"

class WGP_SQUADMGR 
{
	idd = WGP_IDD_SQUADMGR;
	name = "WGP_SQUADMGR";
	movingEnable = false;
	
	controlsBackground[] = 
	{
		WGP_SQUADMGR_Title,
		WGP_SQUADMGR_Background,
		WGP_SQUADMGR_MemberList_Background
	};
	objects[] = {};
	controls[] =
	{
		WGP_SQUADMGR_SubTitle,
		WGP_SQUADMGR_List,
		WGP_SQUADMGR_EditName,		
		WGP_SQUADMGR_ButtonCreate,
		WGP_SQUADMGR_ButtonJoin,
		WGP_SQUADMGR_ButtonLeave,
		WGP_SQUADMGR_ButtonCancel,
        
        WGP_SQUADMGR_Members_List,
        WGP_SQUADMGR_Members_SubTitle
	};
	
	class WGP_SQUADMGR_Background: IGUIBack
	{
		idc = -1;
		x = GRID_ABS_X;
		y = GRID_ABS_Y;
		w = GRID_ABS_W;
		h = GRID_ABS_H + (2 * GRID_H);
	};
    
	class WGP_SQUADMGR_Title : RscTitle
	{
		idc = -1;
		x = GRID_ABS_X;
		y = GRID_ABS_Y - GRID_TITLE_H;
		w = GRID_ABS_W;
		h = GRID_TITLE_H;
		text = "Squad Manager";
	};	
	class WGP_SQUADMGR_SubTitle : RscText
	{
		idc = WGP_IDC_SQUADMGR_SubTitle;
		x = 1 * GRID_W + GRID_ABS_X; 
		y = 0 * GRID_H + GRID_ABS_Y; 
		w = 18 * GRID_W;
		h = 1 * GRID_H;
		text = "Squads";
		colorbackground[] = 
		{
			0,
			0,
			0,
			1
		};		
	};
	
	class WGP_SQUADMGR_List : RscListBox {
		idc = WGP_IDC_SQUADMGR_List;
		x = 1 * GRID_W + GRID_ABS_X;
		y = 1 * GRID_H + GRID_ABS_Y;
		w = 18 * GRID_W;
		h = 17 * GRID_H;
        onLBSelChanged = "execVM 'SQUADMGR\onList_Clicked.sqf'";
	};
	

	class WGP_SQUADMGR_ButtonCreate : RscButtonMenu {
		idc = WGP_IDC_SQUADMGR_ButtonCreate;
		x = 31 * GRID_W + GRID_ABS_X; 
		y = 18.5 * GRID_H + GRID_ABS_Y; 
		w = 8 * GRID_W;
		h = 0.9 * GRID_H;
		text = "Create";
		font = "PuristaMedium";
		action = "execVM 'SQUADMGR\onButtonCreateClicked.sqf';";
	};
	
	class WGP_SQUADMGR_ButtonJoin : RscButtonMenu {
		idc = WGP_IDC_SQUADMGR_ButtonJoin;
		x = 1 * GRID_W + GRID_ABS_X; 
		y = 19.5 * GRID_H + GRID_ABS_Y; 
		w = 8 * GRID_W;
		h = 0.9 * GRID_H;
		text = "Join";
		font = "PuristaMedium";
		action = "execVM 'SQUADMGR\onButtonJoinClicked.sqf';";
	};
	class WGP_SQUADMGR_ButtonLeave : RscButtonMenu {
		idc = WGP_IDC_SQUADMGR_ButtonLeave;
		x = 31 * GRID_W + GRID_ABS_X; 
		y = 19.5 * GRID_H + GRID_ABS_Y; 
		w = 8 * GRID_W;
		h = 0.9 * GRID_H;
		text = "Leave";
		font = "PuristaMedium";
		action = "execVM 'SQUADMGR\onButtonLeaveClicked.sqf';";
	};
    class WGP_SQUADMGR_ButtonCancel : RscButtonMenu
	{
		idc = WGP_IDC_SQUADMGR_ButtonCancel;
		x = 1 * GRID_W + GRID_ABS_X; 
		y = 20.5 * GRID_H + GRID_ABS_Y; 
		w = 8 * GRID_W;
		h = 0.9 * GRID_H;
		text = "Cancel";
		font = "PuristaMedium";
		action = "closeDialog 3";
	};
	
	class WGP_SQUADMGR_EditName : RscEdit
	{
		idc = WGP_IDC_SQUADMGR_EditName;
		x = 1 * GRID_W + GRID_ABS_X; 
		y = 18.5 * GRID_H + GRID_ABS_Y; 
		w = 29 * GRID_W;
		h = 0.9 * GRID_H;
		colorBackground[] = {0,0,0,0.3};
		text = "Group name";
		tooltip = "Define group name";
	};
    
    class WGP_SQUADMGR_Members_SubTitle : RscText
	{
		idc = WGP_IDC_SQUADMGR_Members_SubTitle;
		x = 20 * GRID_W + GRID_ABS_X; 
		y = 0 * GRID_H + GRID_ABS_Y; 
		w = 19 * GRID_W;
		h = 1 * GRID_H;
		text = "Members";
		colorbackground[] = {0,0,0,1};
	};
    
    class WGP_SQUADMGR_Members_List : RscListNBox
	{
		idc = WGP_IDC_SQUADMGR_Members_List;
		x = 20 * GRID_W + GRID_ABS_X; 
		y = 1 * GRID_H + GRID_ABS_Y; 
		w = 19 * GRID_W;
		h = 17 * GRID_H;
        
		color[] = {1,1,1,1};
		colorText[] = {1,1,1,1};
		colorTextSelect[] = {1,1,1,1};
		colorDisabled[] = {1,1,1,1};
		colorScrollbar[] = {1,1,1,1};
		colorSelect[] = {1,1,1,1};
		colorSelect2[] = {1,1,1,1};
		colorBackground[] = {0,0,0,0.3};
		colorBackground2[] = {0,0,0,0.3};
        colorBackgroundActive[] = {0,0,0,0.3};
		colorSelectBackground[] = {0,0,0,0};
		colorSelectBackground2[] = {0,0,0,0};
		colorPicture[] = {1,1,1,1};
		colorPictureSelected[] = {1,1,1,1};
		colorPictureDisabled[] = {1,1,1,1};
        period = 0;
        
		columns[] = {-0.01,0.05,0.9}; // Horizontal coordinates of columns (relative to list width, in range from 0 to 1)
		rowHeight = GRID_H; // Row height
	};
    
    
    
	class WGP_SQUADMGR_MemberList_Background: IGUIBack
	{
		idc = -1;
		x = 20 * GRID_W + GRID_ABS_X; 
		y = 1 * GRID_H + GRID_ABS_Y; 
		w = 19 * GRID_W;
		h = 17 * GRID_H;
		colorBackground[] = {0,0,0,0.3};
	};
};