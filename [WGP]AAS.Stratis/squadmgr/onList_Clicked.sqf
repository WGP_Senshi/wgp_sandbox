#include "defines.hpp";
private["_groups", "_ctrl_members"];
disableSerialization;
_ctrl_members = finddisplay WGP_IDD_SQUADMGR displayctrl WGP_IDC_SQUADMGR_Members_List;
WGP_SQUADMGR_Selection = lbCurSel WGP_IDC_SQUADMGR_List;
lbclear _ctrl_members;
_groups = [];
{
	if (!(group _x in _groups) && side group _x == playerside) then
	{
		private["_group","_CustomGroup","_gname"];
		_group = group _x;
		_CustomGroup = _group getVariable "CustomGroup";
		if (!isNil "_CustomGroup") then
		{
			_groups = _groups + [_group];
		};
	};
} foreach playableUnits;

_group = _groups select WGP_SQUADMGR_Selection;
{
    private["_squadinfo","_string"];
    _squadinfo = squadParams _x select 0;
    _string = name _x;
    if (!isNil "_squadinfo") then {
	    if (count _squadinfo > 0) then {
	    	if (_squadinfo select 0 != "") then { _string = _string + " " + (_squadinfo select 0)};
	    };
    };
	_ctrl_members lnbAddRow ["",_string,"2"];
    if (!isNil "_squadinfo") then {
        if (count _squadinfo > 0) then {
            _ctrl_members lnbSetPicture [[_foreachindex,2], _squadinfo select 4];
        };
    };
    if (vehicle _x != _x) then {
        _icon = getText (configFile >> "CfgVehicles" >> typeof vehicle _x >> "icon" );
        _ctrl_members lnbsetPicture [[_foreachindex, 0], _icon];
    } else {
        switch (side group player) do {
		    case West: { loadouts = loadouts_setup select 0};
		    case East: { loadouts = loadouts_setup select 1};
		    case Resistance: { loadouts = loadouts_setup select 2};
		    default { loadouts = loadouts_setup select 3};
		};
	    if (!isNil {loadouts select (_x getvariable "playerkitid")}) then {
	        _currentkit = loadouts select (_x getvariable "playerkitid");
	        _icon = switch (_currentkit select 1) do {
				case "Squadleader" : {"a3\ui_f\data\map\VehicleIcons\iconmanofficer_ca.paa"};
				case "FTLeader" : {"a3\ui_f\data\map\VehicleIcons\iconmanleader_ca.paa"};
				case "Medic" : {"a3\ui_f\data\map\VehicleIcons\iconmanmedic_ca.paa"};
				case "LAT" : {"a3\ui_f\data\map\VehicleIcons\iconmanat_ca.paa"};
				case "HAT" : {"a3\ui_f\data\map\VehicleIcons\iconmanexplosive_ca.paa"};
				case "AR" : {"a3\ui_f\data\map\VehicleIcons\iconmanmg_ca.paa"};
				case "MG" : {"a3\ui_f\data\map\VehicleIcons\iconmanmg_ca.paa"};
				case "Grenadier" : {"a3\ui_f\data\map\VehicleIcons\iconmanvirtual_ca.paa"};
				case "Sniper" : {"a3\ui_f\data\map\VehicleIcons\iconmanrecon_ca.paa"};
				case "Spotter" : {"a3\ui_f\data\map\VehicleIcons\iconmanrecon_ca.paa"};
				case "Engineer" : {"a3\ui_f\data\map\VehicleIcons\iconmanengineer_ca.paa"};
				default {"a3\ui_f\data\map\VehicleIcons\iconman_ca.paa"};
            };
	        _ctrl_members lnbsetPicture [[_foreachindex, 0], _icon];
	        
	    };
    };
} foreach units _group;