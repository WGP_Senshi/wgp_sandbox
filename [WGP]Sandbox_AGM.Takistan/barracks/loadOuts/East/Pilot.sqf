comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "rhs_uniform_mflora_patchless";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
this addItemToUniform "SmokeShellRed";
this addItemToUniform "AGM_EarBuds";
this addVest "rhs_vest_commander";
for "_i" from 1 to 3 do {this addItemToVest "16Rnd_9x21_Mag";};
this addHeadgear "rhs_zsh7a_mike";
this addGoggles "G_Aviator";

comment "Add weapons";
this addWeapon "hgun_Rook40_F";
this addWeapon "Binocular";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "tf_fadak";
this linkItem "ItemGPS";
this linkItem "ItemWatch";
this setVariable ["AGM_GForceCoef", 0.75];