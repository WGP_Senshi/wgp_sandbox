comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "rhs_uniform_mflora_patchless";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
for "_i" from 1 to 2 do {this addItemToUniform "rhs_mag_rdg2_white";};
this addItemToUniform "AGM_EarBuds";
this addVest "rhs_6b23_ML_6sh92";
this addItemToVest "rhs_mag_rgd5";
for "_i" from 1 to 8 do {this addItemToVest "rhs_30Rnd_545x39_7N10_AK";};
this addBackpack "rhs_rpg_empty";
for "_i" from 1 to 4 do {this addItemToBackpack "rhs_rpg7_PG7VL_mag";};
this addHeadgear "rhs_6b27m_green_ess";

comment "Add weapons";
this addWeapon "rhs_weap_ak74m";
this addWeapon "rhs_weap_rpg7";
this addSecondaryWeaponItem "rhs_acc_pgo7v";
this addWeapon "Binocular";

this addItemToVest "rhs_30Rnd_545x39_7N10_AK";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "tf_fadak";
this linkItem "ItemWatch";