comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "rhs_uniform_cu_ocp_patchless";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
for "_i" from 1 to 2 do {this addItemToUniform "rhs_mag_an_m8hc";};
this addItemToUniform "AGM_EarBuds";
this addVest "rhsusf_iotv_ocp_SAW";
this addHeadgear "rhsusf_ach_helmet_ESS_ocp";

this addItemToVest "rhsusf_100Rnd_762x51";

comment "Add weapons";
this addWeapon "rhs_weap_m240B";
this addWeapon "Binocular";

this addItemToVest "rhsusf_100Rnd_762x51";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "tf_anprc152";
this linkItem "ItemWatch";