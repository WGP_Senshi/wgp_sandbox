private["_tmp"];

_radios = player call TFAR_fnc_radiosList;
if (count _radios > 0) then
{
	(_radios select 0) call TFAR_fnc_setActiveSwRadio;
	[(call TFAR_fnc_activeSwRadio), 1, "101"] call TFAR_fnc_SetChannelFrequency;
	[(call TFAR_fnc_activeSwRadio), 2, "102"] call TFAR_fnc_SetChannelFrequency;
	[(call TFAR_fnc_activeSwRadio), 3, "103"] call TFAR_fnc_SetChannelFrequency;
	[(call TFAR_fnc_activeSwRadio), 4, "104"] call TFAR_fnc_SetChannelFrequency;
	[(call TFAR_fnc_activeSwRadio), 5, "105"] call TFAR_fnc_SetChannelFrequency;
	[(call TFAR_fnc_activeSwRadio), 6, "106"] call TFAR_fnc_SetChannelFrequency;
	[(call TFAR_fnc_activeSwRadio), 7, "107"] call TFAR_fnc_SetChannelFrequency;
	[(call TFAR_fnc_activeSwRadio), 8, "108"] call TFAR_fnc_SetChannelFrequency;
	_tmp  = [(call TFAR_fnc_activeSwRadio), 3] call TFAR_fnc_setAdditionalSwChannel;
	player sidechat "INFO: Funkfrequenzen eingestellt";
}
else
{
	player sidechat "INFO: Kein Funkgerät gefunden";
};