
if (pixTfarZeusfix_SwPressed) then
{
	if (_this select 1 == TF_tangent_sw_scancode) then
	{
		if (str(_this select 2) == str(TF_tangent_sw_modifiers select 0)) then
		{
			if (str(_this select 3) == str(TF_tangent_sw_modifiers select 1)) then
			{
				if (str(_this select 4) == str(TF_tangent_sw_modifiers select 2)) then 
				{
					pixTfarZeusfix_SwPressed = false;
					_tmp = [] call TFAR_fnc_onSwTangentReleased;
				};
			};
		};
	};

	if (_this select 1 == TF_tangent_additional_sw_scancode) then
	{
		if (str(_this select 2) == str(TF_tangent_additional_sw_modifiers select 0)) then
		{
			if (str(_this select 3) == str(TF_tangent_additional_sw_modifiers select 1)) then
			{
				if (str(_this select 4) == str(TF_tangent_additional_sw_modifiers select 2)) then 
				{
					pixTfarZeusfix_SwPressed = false;
					_tmp = [] call TFAR_fnc_onAdditionalSwTangentReleased;
				};
			};
		};
	};
};


if (pixTfarZeusfix_LrPressed) then
{
	if (_this select 1 == TF_tangent_lr_scancode) then
	{
		if (str(_this select 2) == str(TF_tangent_lr_modifiers select 0)) then
		{
			if (str(_this select 3) == str(TF_tangent_lr_modifiers select 1)) then
			{
				if (str(_this select 4) == str(TF_tangent_lr_modifiers select 2)) then 
				{
					pixTfarZeusfix_LrPressed = false;
					_tmp = [] call TFAR_fnc_onLrTangentReleased;
				};
			};
		};
	};
};


if (_this select 1 == TF_sw_channel_1_scancode) then
{
	if (str(_this select 2) == str(TF_sw_channel_1_modifiers select 0)) then
	{
		if (str(_this select 3) == str(TF_sw_channel_1_modifiers select 1)) then
		{
			if (str(_this select 4) == str(TF_sw_channel_1_modifiers select 2)) then 
			{
				[0] call TFAR_fnc_processSWChannelKeys;
			};
		};
	};
};
if (_this select 1 == TF_sw_channel_2_scancode) then
{
	if (str(_this select 2) == str(TF_sw_channel_2_modifiers select 0)) then
	{
		if (str(_this select 3) == str(TF_sw_channel_2_modifiers select 1)) then
		{
			if (str(_this select 4) == str(TF_sw_channel_2_modifiers select 2)) then 
			{
				[1] call TFAR_fnc_processSWChannelKeys;
			};
		};
	};
};
if (_this select 1 == TF_sw_channel_3_scancode) then
{
	if (str(_this select 2) == str(TF_sw_channel_3_modifiers select 0)) then
	{
		if (str(_this select 3) == str(TF_sw_channel_3_modifiers select 1)) then
		{
			if (str(_this select 4) == str(TF_sw_channel_3_modifiers select 2)) then 
			{
				[2] call TFAR_fnc_processSWChannelKeys;
			};
		};
	};
};
if (_this select 1 == TF_sw_channel_4_scancode) then
{
	if (str(_this select 2) == str(TF_sw_channel_4_modifiers select 0)) then
	{
		if (str(_this select 3) == str(TF_sw_channel_4_modifiers select 1)) then
		{
			if (str(_this select 4) == str(TF_sw_channel_4_modifiers select 2)) then 
			{
				[3] call TFAR_fnc_processSWChannelKeys;
			};
		};
	};
};	