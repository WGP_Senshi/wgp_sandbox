class WGP
{
	tag = "wgp";
	
	class mp
	{
		file = "mp\fnc";
		class onPlayerConnected {description = "WGP MP: Handles JIP event";}; // Function name:  wgp_fnc_onPlayerConnected
		class setRank {description = "WGP MP: Sets Rank";}; // Function name:  wgp_fnc_setRank
		class setGroupID {description = "WGP MP: Sets group ID";}; // Function name:  wgp_fnc_setGroupID
        class cleanBase {description = "WGP MP: Removes all dead units and vehicles from a marker/trigger area";};
        class preBrief {description = "WGP MP: Sets up various stuff that has to be done BEFORE the briefing."; recompile = 1; preInit = 1;};
	};
    
    class curator { // Inspired by Fett_Li - http://forums.bistudio.com/showthread.php?176691-Making-placed-units-be-editable-for-every-Zeus
        file = "mp\curator";
        class eventHandlers {description = "WGP CURATOR: Triggers on object/group placement for all Zeus"; postInit = 1;};
        class objPlaced {description = "WGP CURATOR: Makes Zeus-placed objects editable";};
        class grpPlaced {description = "WGP CURATOR: Makes Zeus-placed groups editable";};
        class isZeus {description = "WGP CURATOR: Boolean test if player is Zeus";};
    };
	
	class resupply
	{
		file = "mp\resupply";
		class resupply {description = "WGP Resupply: Resupplies vehicle (fuel, ammo, damage)";}; // Function name:  wgp_fnc_onPlayerConnected
	};

};