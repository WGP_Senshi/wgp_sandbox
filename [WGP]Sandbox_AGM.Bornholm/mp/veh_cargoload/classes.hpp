#include "defines.hpp"

class WGP_VEH_CargoLoad 
{
	idd = WGP_VEH_CargoLoad_IDD;
	name = "WGP_VEH_CargoLoad";
	movingEnable = false;
	
	controlsBackground[] = 
	{
		WGP_VEH_CargoLoad_Title,
		WGP_VEH_CargoLoad_Background
	};
	objects[] = {};
	controls[] =
	{
		WGP_VEH_CargoLoad_LISTNBOX,
		WGP_VEH_CargoLoad_LISTNBOX_RIGHT,
		WGP_VEH_CargoLoad_LISTNBOX_LEFT,
		WGP_VEH_CargoLoad_MassBar,
		
		WGP_VEH_CargoLoad_ButtonOK,
		WGP_VEH_CargoLoad_ButtonCancel
	};
	
	class WGP_VEH_CargoLoad_Background: IGUIBack
	{
		idc = -1;
		x = GRID_ABS_X /2 ;
		y = GRID_ABS_Y;
		w = GRID_ABS_W;
		h = GRID_ABS_H;
	};
    
	class WGP_VEH_CargoLoad_Title : RscTitle
	{
		idc = WGP_VEH_CargoLoad_IDC_Title;
		x = GRID_ABS_X /2 ;
		y = GRID_ABS_Y - GRID_TITLE_H;
		w = GRID_ABS_W;
		h = GRID_TITLE_H;
		text = "Vehicle Cargo";
	};
	class WGP_VEH_CargoLoad_MassBar : RscProgress
	{
		idc = WGP_VEH_CargoLoad_IDC_MassBar;
		tooltip = " Unknown / Infinity";
		colorFrame[] = {0,0,0,1};
		colorBar[] = {1,1,1,1};
		x = GRID_ABS_X /2 ;
		y = GRID_ABS_Y;
		w = GRID_ABS_W;
		h = GRID_TITLE_H;
	};	

	class WGP_VEH_CargoLoad_ButtonOK : RscButtonMenuOK
	{
		idc = WGP_VEH_CargoLoad_IDC_ButtonOK;
		x = 1 * GRID_W + GRID_ABS_X /2 ; 
		y = 19 * GRID_H + GRID_ABS_Y; 
		w = 8.9 * GRID_W;
		h = 0.9 * GRID_H;
		text = "OK";
		font = "PuristaMedium";
		action = "closeDialog 1";
	};
	
	class WGP_VEH_CargoLoad_ButtonCancel : RscButtonMenuCancel
	{
		idc = WGP_VEH_CargoLoad_IDC_ButtonCancel;
		x = 10 * GRID_W + GRID_ABS_X / 2; 
		y = 19 * GRID_H + GRID_ABS_Y; 
		w = 9 * GRID_W;
		h = 0.9 * GRID_H;
		text = "Close";
		font = "PuristaMedium";
		action = "closeDialog 2";
        
	};
	
	class WGP_VEH_CargoLoad_LISTNBOX_RIGHT: RscButton
	{
		idc = WGP_VEH_CargoLoad_LISTNBOX_RIGHT_IDC;
		text = "+";
		borderSize = 0;
		colorShadow[] = {0,0,0,0};
		action = "1 call compile preprocessFileLineNumbers ""mp\veh_cargoload\fn_addItem.sqf""";
	};
	class WGP_VEH_CargoLoad_LISTNBOX_LEFT: WGP_VEH_CargoLoad_LISTNBOX_RIGHT
	{
		idc = WGP_VEH_CargoLoad_LISTNBOX_LEFT_IDC;
		text = "-";
		action = "-1 call compile preprocessFileLineNumbers ""mp\veh_cargoload\fn_addItem.sqf""";
	};
	
    class WGP_VEH_CargoLoad_LISTNBOX : RscListNBox
	{
		idc = WGP_VEH_CargoLoad_LISTNBOX_IDC; // Control identification (without it, the control won't be displayed)
		type = CT_LISTNBOX; // Type 102
		// style = ST_LEFT + LB_TEXTURES; // Style
		style = ST_LEFT; // Style

		x = 0 * GRID_W + GRID_ABS_X /2 ; 
		y = 1 * GRID_H + GRID_ABS_Y; 
		w = 40 * GRID_W;
		h = 17.5 * GRID_H;

		sizeEx = GRID_H; // Text size
		font = "PuristaMedium"; // Font from CfgFontFamilies
		color[] = {0.95,0.95,0.95,1};
		colorText[] = {1,1,1,1.0};
		colorDisabled[] = {1,1,1,0.25};
		colorScrollbar[] = {0.95,0.95,0.95,1};
		colorSelect[] = {0,0,0,1};
		colorSelect2[] = {0,0,0,1};
		colorSelectBackground[] = {0.95,0.95,0.95,1};
		colorSelectBackground2[] = {1,1,1,0.5};
		colorPicture[] = {1,1,1,1};
		colorPictureSelected[] = {1,1,1,1};
		colorPictureDisabled[] = {1,1,1,1};

		tooltipColorText[] = {1,1,1,1};
		tooltipColorBox[] = {1,1,1,1};
		tooltipColorShade[] = {0,0,0,0.65};

		columns[] = {0.05,0.15,0.8}; // Horizontal coordinates of columns (relative to list width, in range from 0 to 1)

		drawSideArrows = 1; // 1 to draw buttons linked by idcLeft and idcRight on both sides of selected line. They are resized to line height
		idcLeft = WGP_VEH_CargoLoad_LISTNBOX_LEFT_IDC; // Left button IDC
		idcRight = WGP_VEH_CargoLoad_LISTNBOX_RIGHT_IDC; // Right button IDC


		rowHeight = GRID_H; // Row height
        period = 1.2;
		class ListScrollBar: ScrollBar{};
		class ScrollBar: ScrollBar{};
		

		soundSelect[] = {"\A3\ui_f\data\sound\RscListbox\soundSelect",0.09,1}; // Sound played when an item is selected

	};
};