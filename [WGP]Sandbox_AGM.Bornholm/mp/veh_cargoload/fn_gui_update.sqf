#include "defines.hpp"

private ["_capacity", "_totalmass", "_fullinv_unique"];
_capacity = [vehicle player] call wgp_fnc_getCapacity;
_totalmass = 0;
{_totalmass = _totalmass + ([_x] call wgp_fnc_getItemMass); } foreach (weaponcargo vehicle player);
{_totalmass = _totalmass + ([_x] call wgp_fnc_getItemMass); } foreach (itemcargo vehicle player);
{_totalmass = _totalmass + ([_x] call wgp_fnc_getItemMass); } foreach (magazinecargo vehicle player);
{_totalmass = _totalmass + ([_x] call wgp_fnc_getItemMass); } foreach (backpackcargo vehicle player);
if (_capacity != 0) then {
	findDisplay WGP_VEH_CargoLoad_IDD displayCtrl WGP_VEH_CargoLoad_IDC_MassBar progressSetPosition (_totalmass / _capacity);
} else {
    findDisplay WGP_VEH_CargoLoad_IDD displayCtrl WGP_VEH_CargoLoad_IDC_MassBar progressSetPosition (1);
};
findDisplay WGP_VEH_CargoLoad_IDD displayCtrl WGP_VEH_CargoLoad_IDC_MassBar ctrlSetTooltip ( ([_totalmass] call wgp_fnc_str_formatWeight) + " of " + ([_capacity] call wgp_fnc_str_formatWeight));

wgp_vehcargoload_inventory = itemcargo vehicle player + weaponcargo vehicle player + magazinecargo vehicle player + backpackcargo vehicle player;
_fullinv_unique = (wgp_vehcargoload_items + wgp_vehcargoload_inventory) call wgp_fnc_uniquearray;


_CT_LISTNBOX = findDisplay WGP_VEH_CargoLoad_IDD displayCtrl WGP_VEH_CargoLoad_LISTNBOX_IDC;
lnbClear _CT_LISTNBOX;


wgp_vehcargoload_inventory_unique = [];
{
    private ["_code", "_item", "_count"];
    _code = _x;
    _item = _x call wgp_fnc_findClass;
    wgp_vehcargoload_inventory_unique = wgp_vehcargoload_inventory_unique + [ _code];
    _count = count([wgp_vehcargoload_inventory, {_x == _code}] call BIS_fnc_conditionalSelect);
    if (_code != "") then {
	    _CT_LISTNBOX lnbAddRow [str _count + "x", getText (_item >> "displayname"), [[_x] call wgp_fnc_getItemMass, "kg", 2] call wgp_fnc_str_formatWeight];
	    _CT_LISTNBOX lnbSetPicture[[_foreachindex,1], getText(_item >> "picture")];
    };

    if ((wgp_vehcargoload_items find _code) == -1) then {
	    _CT_LISTNBOX lnbsetColor[[_foreachindex,1],[1, 0, 0, 1]];
	    _CT_LISTNBOX lnbsetColor[[_foreachindex,0],[1, 0, 0, 1]];
	    _CT_LISTNBOX lnbsetColor[[_foreachindex,2],[1, 0, 0, 1]];
        _CT_LISTNBOX lbsetToolTip [_foreachindex, "WARNING: You can remove, but not add this item!"];
        
    };
} foreach _fullinv_unique;