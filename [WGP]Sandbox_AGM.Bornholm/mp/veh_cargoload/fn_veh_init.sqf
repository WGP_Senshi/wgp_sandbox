#include "defines.hpp"

switch (side group player) do {
    case West: { loadouts = loadouts_setup select 0};
    case East: { loadouts = loadouts_setup select 1};
    case Resistance: { loadouts = loadouts_setup select 2};
    default { loadouts = loadouts_setup select 3};
};
_wgp_vehcargoload_loadouts = [];
{_wgp_vehcargoload_loadouts set [count _wgp_vehcargoload_loadouts, (_x select 1)]} foreach loadouts;

wgp_vehcargoload_items = [];
{
    private ["_inv"];
    _inv = call compile preprocessFileLineNumbers ("barracks\loadouts\" + str (side group player) + "\" + _x + "_c.sqf");
    {wgp_vehcargoload_items set [count wgp_vehcargoload_items, _x select 0]; } foreach _inv;
} foreach _wgp_vehcargoload_loadouts;
wgp_vehcargoload_items = wgp_vehcargoload_items call wgp_fnc_uniquearray;

disableSerialization;

createDialog "WGP_VEH_CargoLoad";
call wgp_fnc_gui_update;
findDisplay WGP_VEH_CargoLoad_IDD displayCtrl WGP_VEH_CargoLoad_LISTNBOX_IDC lnbSetCurSelRow 0;