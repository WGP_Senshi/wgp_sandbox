//Einstellbare Variablen
wgp_ai_aimingaccuracy = 0.4;
wgp_ai_aimingShake = 1;
wgp_ai_aimingSpeed = 1;
wgp_ai_spotDistance = 1;
wgp_ai_spotTime = 1;
wgp_ai_courage = 1;
wgp_ai_reloadSpeed = 1;
wgp_ai_commanding = 1;

waitUntil {!isNull player};
waitUntil {!isNull ZeusModule1};
waitUntil {alive player};

if(player call wgp_fnc_isZeus) then {
	_curator =  getAssignedCuratorLogic player;
	_curator addEventHandler ["CuratorObjectPlaced",{_this select 1 setSkill 1}];
	_curator addEventHandler ["CuratorObjectPlaced",{_this select 1 setSkill ["aimingAccuracy",wgp_ai_aimingaccuracy]}];
	_curator addEventHandler ["CuratorObjectPlaced",{_this select 1 setSkill ["aimingShake",wgp_ai_aimingShake]}];
	_curator addEventHandler ["CuratorObjectPlaced",{_this select 1 setSkill ["aimingSpeed",wgp_ai_aimingSpeed]}];
	_curator addEventHandler ["CuratorObjectPlaced",{_this select 1 setSkill ["spotDistance",wgp_ai_spotDistance]}];
	_curator addEventHandler ["CuratorObjectPlaced",{_this select 1 setSkill ["spotTime",wgp_ai_spotTime]}];
	_curator addEventHandler ["CuratorObjectPlaced",{_this select 1 setSkill ["courage",wgp_ai_courage]}];
	_curator addEventHandler ["CuratorObjectPlaced",{_this select 1 setSkill ["reloadSpeed",wgp_ai_reloadSpeed]}];
	_curator addEventHandler ["CuratorObjectPlaced",{_this select 1 setSkill ["commanding",wgp_ai_commanding]}];
	[_curator,"object",	["Unitpos","Rank","Damage","Fuel","Lock","RespawnVehicle","RespawnPosition","exec"]] call BIS_fnc_setCuratorAttributes;
	[_curator,"group",["GroupID","Behaviour","Formation"]] call BIS_fnc_setCuratorAttributes;
};