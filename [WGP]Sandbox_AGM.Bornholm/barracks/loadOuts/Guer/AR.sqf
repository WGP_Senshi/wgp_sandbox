comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "BWA3_Uniform_Fleck";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
this addItemToUniform "AGM_EarBuds";
for "_i" from 1 to 2 do {this addItemToUniform "BWA3_DM25";};
this addVest "BWA3_Vest_Autorifleman_Fleck";
for "_i" from 1 to 3 do {this addItemToVest "BWA3_200Rnd_556x45";};
this addHeadgear "BWA3_M92_Fleck";

comment "Add weapons";
this addWeapon "BWA3_MG4";
this addPrimaryWeaponItem "BWA3_optic_RSAS";
this addWeapon "Binocular";
this addItemToVest "BWA3_200Rnd_556x45";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "tf_anprc152_3";