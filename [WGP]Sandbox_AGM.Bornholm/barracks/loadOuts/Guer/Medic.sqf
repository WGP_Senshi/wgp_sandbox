comment "Exported from Arsenal by [WGP]Senshi";

comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "BWA3_Uniform_Fleck";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
this addItemToUniform "AGM_EarBuds";
for "_i" from 1 to 4 do {this addItemToUniform "BWA3_DM25";};
this addItemToUniform "BWA3_30Rnd_556x45_G36";
this addVest "BWA3_Vest_Medic_Fleck";
for "_i" from 1 to 8 do {this addItemToVest "BWA3_30Rnd_556x45_G36";};
this addItemToVest "BWA3_DM51A1";
this addBackpack "B_AssaultPack_rgr";
for "_i" from 1 to 20 do {this addItemToBackpack "AGM_Bandage";};
for "_i" from 1 to 8 do {this addItemToBackpack "AGM_Morphine";};
for "_i" from 1 to 8 do {this addItemToBackpack "AGM_Epipen";};
for "_i" from 1 to 4 do {this addItemToBackpack "AGM_Bloodbag";};
this addHeadgear "BWA3_M92_Fleck";

comment "Add weapons";
this addWeapon "BWA3_G36";
this addPrimaryWeaponItem "BWA3_acc_LLM01_flash";
this addPrimaryWeaponItem "BWA3_optic_RSAS";
this addWeapon "Binocular";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "tf_anprc152_10";

this setVariable ["AGM_IsMedic", true];