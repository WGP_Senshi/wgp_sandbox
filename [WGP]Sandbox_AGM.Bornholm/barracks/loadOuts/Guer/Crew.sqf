comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "BWA3_Uniform_Crew_Fleck";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
this addItemToUniform "AGM_EarBuds";
this addVest "BWA3_Vest_Rifleman1_Fleck";
for "_i" from 1 to 3 do {this addItemToVest "BWA3_40Rnd_46x30_MP7";};
this addHeadgear "BWA3_CrewmanKSK_Fleck_Headset";

comment "Add weapons";
this addWeapon "BWA3_MP7";
this addHandgunItem "BWA3_acc_LLM01_flash";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemGPS";