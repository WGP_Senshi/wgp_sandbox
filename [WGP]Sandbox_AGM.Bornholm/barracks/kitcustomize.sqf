if (player getVariable "playerKitID" != -1) then {
    
    _dummy = "Box_NATO_Ammo_F" createVehicleLocal getpos player;
    _dummy setPos  (player modelToWorld [0,-1,0]);
	_dummy hideObject true;
    // waituntil {!isnull _dummy};
    // Clean and fill the crate
    clearWeaponCargo _dummy;
    clearBackpackCargo _dummy;
	clearMagazineCargo _dummy;
	clearItemCargo _dummy;
	// _dummy allowDamage false;
	
	{
	    if(_x select 0 iskindof "Bag_Base") then {	
		_dummy addBackpackCargo [_x select 0, 1];
		}
		else {
		_dummy addItemCargo [_x select 0, 10];
		};
	} foreach call compile preprocessFileLineNumbers ("barracks\loadouts\" + str (side player) + "\" + (loadouts select (player getVariable "playerKitID") select 1) + "_c.sqf");

    // Eventhandlers for taking, putting items and closing the crate
    /*
    _id2 = player addEventHandler["Take", {
        _container = _this select 1;
        _item = _this select 2;
	    if(_item iskindof "Bag_Base") then {
			_container addBackpackCargo [_item, 1];
		}
		else {
			_container addItemCargo [_item, 1];
		};
    } ];
    _id3 = player addEventHandler["Put", {
        _container = _this select 1;
        _item = _this select 2;
        clearWeaponCargo _container;
		clearMagazineCargo _container;
		clearItemCargo _container;
  		clearBackpackCargo _container;
        {
		    if(_x select 0 iskindof "Bag_Base") then {	
				_container addBackpackCargo [_x select 0, 1];
			}
			else {
				_container addItemCargo [_x select 0, 10];
			};
		} foreach call compile preprocessFileLineNumbers ("barracks\loadouts\" + str (side player) + "\" + (loadouts select (player getVariable "playerKitID") select 1) + "_c.sqf");
    } ];
    */
	_id = _dummy addEventHandler ["ContainerClosed", {
        _container = _this select 0;
	    clearWeaponCargo _container;
		clearMagazineCargo _container;
		clearItemCargo _container;
    	clearBackpackCargo _container;
	    _container removeAllEventHandlers "ContainerClosed";
        // player removeAllEventHandlers "Take";
        // player removeAllEventHandlers "Put";
	    // deleteVehicle (_container);
	}];
    
    // Only now we open the crate
	player action ["GEAR",_dummy]; // open weapon crate
} else {
    systemchat "INFO: You have to select a kit before you can customize it.";
};