private ["_logic", "_units", "_activated"];
#include "\ares_zeusExtensions\module_header.hpp"

_unitUnderCursor = [_logic, false] call Ares_fnc_GetUnitUnderCursor;

if (isNull _unitUnderCursor) then {
	[objnull, format ["ERROR: Must be placed on unit"]] call bis_fnc_showCuratorFeedbackMessage;
}else {
	_dialogResult = [
		"Set Combat Mode",
		[
			["Change only:", ["Unit", "Group"], 1],
			["Mode:", ["Never Fire", "Hold Fire", "Hold Fire, Engage", "Fire at Will","Fire at Will, Engage"]]
		]
	] call Ares_fnc_ShowChooseDialog;
	
	if (count _dialogResult > 0) then {
		_dialogUnitsToAffect = _dialogResult select 0;
		_dialogMode = _dialogResult select 1;

		private ["_units", "_mode"];
		_units = [];
		switch (_dialogUnitsToAffect) do {
			case 0: {_units = crew vehicle _unitUnderCursor};
			case 1: {_units = group _unitUnderCursor};
		};

		switch (_dialogMode) do {
			case 0: {_mode = "BLUE"};
			case 1: {_mode = "GREEN"};
			case 2: {_mode = "WHITE"};
			case 3: {_mode = "YELLOW"};
			case 4: {_mode = "RED"};
		};
		
_units setCombatMode _mode;
[objnull, format ["Units set to Combat Mode %1", _mode]] call bis_fnc_showCuratorFeedbackMessage;
	};
};

#include "\ares_zeusExtensions\module_footer.hpp"