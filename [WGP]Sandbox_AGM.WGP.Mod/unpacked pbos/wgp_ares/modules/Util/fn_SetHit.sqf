private ["_logic", "_units", "_activated"];
#include "\ares_zeusExtensions\module_header.hpp"

_unitUnderCursor = [_logic, false] call Ares_fnc_GetUnitUnderCursor;

/*
This sets part-specific damage, not overall damage.
CAUTION: Part-specific damage has NO relation to overall "damage". You can set all modules to be destroyed, but the vehicle damage would still be zero.
This is great to simulate not-burned-out "wrecks", but can cause issues when attempting to repair (most repair functions expect the vehicle to be at least minimally damaged).
If you damage a playable vehicle, make sure to give it at least minor overall damage as well.

*/
if (isNull _unitUnderCursor) then {
	[objnull, format ["ERROR: Must be placed on a unit"]] call bis_fnc_showCuratorFeedbackMessage;
} else {
	_hitpoints =  (configfile >> "CfgVehicles" >> typeof _unitUnderCursor >> "Hitpoints");
	_arr_hp = [];
	for "_i" from 0 to ((count _hitpoints) - 1) do
	{
	     _arr_hp set [count _arr_hp, configname(_hitpoints select _i)];
	};
	_string = "";
	{
		_string = _string + format [
			"<t align='left'>%1</t><t align='right'>%2</t><br/>", 
			_x,
			str(_unitUnderCursor getHitPointDamage (_arr_hp select _forEachIndex))
		]; 
	} forEach _arr_hp;
	
	hintSilent parseText _string;


	_dialogResult = [
		"Set damage",
		[
			["of :", _arr_hp, 1],
			["to :", ["0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1"]]
		]
	] call Ares_fnc_ShowChooseDialog;
	


	if (count _dialogResult > 0) then {
		_dialogHitPoint = _dialogResult select 0;
		_dialogDamage = _dialogResult select 1;
		_hp = _arr_hp select _dialogHitPoint;
		_dmg = _dialogDamage / 10;
		_unitUnderCursor setHitPointDamage [_hp, _dmg];
		[objnull, format ["%1 damage set to %2", _hp, _dmg]] call bis_fnc_showCuratorFeedbackMessage;
	};
};

#include "\ares_zeusExtensions\module_footer.hpp"