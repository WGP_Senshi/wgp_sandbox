private ["_logic", "_units", "_activated"];
#include "\ares_zeusExtensions\module_header.hpp"


_unitUnderCursor = [_logic, false] call Ares_fnc_GetUnitUnderCursor;

_model = objNull;

if (isNull _unitUnderCursor) then {
	_model = "Land_PaperBox_open_empty_F" createVehicle (position _logic);
[[_model]] call Ares_fnc_AddUnitsToCurator;
} else {
	_model = _unitUnderCursor;
};

_dialogResult = [
	"Add Kit ...",
	[
		["Selection:", ["Yes", "No"], 0],
		["Customization:", ["Yes", "No"], 0]
	]
] call Ares_fnc_ShowChooseDialog;

if (count _dialogResult > 0) then {
	_dialogQSelect = _dialogResult select 0;
	_dialogQCustom = _dialogResult select 1;

	if (_dialogQSelect == 0) then {
		[[_model, ["Kit Selection", "barracks\showModal.sqf"]], "addAction", true] call BIS_fnc_MP;		
	};

	if (_dialogQCustom == 0) then {
		[[_model, ["Kit Customization", "barracks\kitcustomize.sqf"]], "addAction", true] call BIS_fnc_MP;
	};
	[objnull, format ["%1 created ", "Kit crate"]] call bis_fnc_showCuratorFeedbackMessage;
};


#include "\ares_zeusExtensions\module_footer.hpp"