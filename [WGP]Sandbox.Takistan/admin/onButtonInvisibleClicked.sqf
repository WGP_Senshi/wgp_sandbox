closeDialog 0;

// Es gibt keine Funktion "isInvisible", deshalb Workaround mit player-Variablen
if (player getVariable ["invisible", false]) then {
    player setVariable["invisible", false];
    PV_invisible = [vehicle player, false];
    publicVariableServer "PV_invisible";
    player sidechat "INFO: Du bist sichtbar!";
} else {
    player setVariable["invisible", true];
    PV_invisible = [vehicle player, true];
    publicVariableServer "PV_invisible";
    player sidechat "INFO: Du bist unsichtbar.";
    
};