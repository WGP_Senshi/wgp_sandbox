#include "defines.hpp"
pixLogisticDialogBarrack_ButtonOK = 1; 
pixLogisticDialogBarrack_Selection = lbCurSel PIXLOGISTIC_IDC_DIALOG_BARRACK_List; 

private["_scriptFilename"];
_scriptFilename = (loadouts select pixLogisticDialogBarrack_Selection) select 1;
systemchat ("INFO: Kit equipped: " + ((loadouts select pixLogisticDialogBarrack_Selection) select 0));
this = player;
call compile preprocessFileLineNumbers ("barracks\loadouts\" + str playerside + "\" + _scriptFilename + ".sqf");
this = Nil;
player setVariable ["playerKitID", pixLogisticDialogBarrack_Selection]; // player has to remember what kit he has. E.g. for kit customization.
closeDialog 0;
call compile preprocessFileLineNumbers ("barracks\kitcustomize.sqf");