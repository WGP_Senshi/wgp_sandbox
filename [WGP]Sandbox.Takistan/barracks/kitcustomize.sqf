if (player getVariable "playerKitID" != -1) then {
    
	_dummy = "Box_NATO_AmmoVeh_F" createVehicleLocal getPos player;
	_dummy hideObject true;
    waituntil {!isnull _dummy};
    _id2 = player addEventHandler["Take", {
        _container = _this select 1;
        _item = _this select 2;
        _container addItemCargo [_item, 1];
    } ];
    _id3 = player addEventHandler["Put", {
        _container = _this select 1;
        _item = _this select 2;
        clearWeaponCargo _container;
		clearMagazineCargo _container;
		clearItemCargo _container;
        {
	   		_container addItemCargo [_x select 0, 10];
		} foreach call compile preprocessFileLineNumbers ("barracks\loadouts\" + str playerside + "\" + (loadouts select (player getVariable "playerKitID") select 1) + "_c.sqf");
    } ];
	_id = _dummy addEventHandler ["ContainerClosed", {
	    _dummy removeAllEventHandlers "ContainerClosed";
	    deleteVehicle (_dummy);
        player removeAllEventHandlers "Take";
        player removeAllEventHandlers "Put";
	}];
    clearWeaponCargo _dummy;
	clearMagazineCargo _dummy;
	clearItemCargo _dummy;
	_dummy allowDamage false;
	
	{
	    if(_x select 0 iskindof "Bag_Base") then {	
		_dummy addBackpackCargo [_x select 0, 1];
		}
		else {
		_dummy addItemCargo [_x select 0, 10];
		};
	} foreach call compile preprocessFileLineNumbers ("barracks\loadouts\" + str playerside + "\" + (loadouts select (player getVariable "playerKitID") select 1) + "_c.sqf");
	player action ["GEAR",_dummy]; // open weapon crate
} else {
    systemchat "INFO: You have to select a kit before you can customize it.";
};