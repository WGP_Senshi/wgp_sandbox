private["_building"];
_building = _this select 0;

/*-----------------------------------------------------------------	**/
/* Dialog erstellen*/
#include "defines.hpp";
createDialog "PIXLOGISTIC_DIALOG_BARRACK";
// ctrlEnable [PIXLOGISTIC_IDC_DIALOG_BARRACK_Weapons_List,false];

// private["_loadouts"]; 
// Loadouts = West, East, Guerilla
loadouts = [
	[
		["Squad Leader","W_SL"],
		["Radio Operator","W_RO"],
		["Fireteam Leader","W_FTL"],
		["Medic","W_Medic"],
		["Automatic Rifleman","W_AR"],
		["Rifleman","W_R"],
		["Grenadier","W_Gren"],
		["Marksman","W_Marks"],
		["Light Anti-Tank","W_LAT"],
		["Engineer","W_Engi"],
		["Machinegunner","W_MG"],
		["Sniper","W_Sniper"],
		["Spotter","W_Spotter"],
		["UAV Operator","W_UAVO"],
		["Crewman","W_Crew"],
		["Pilot","W_Pilot"]
    ],
    [
		["Rifleman","Rifleman"],
		["Operator","Rifleman"]
    ],
    [
		["Rifleman","Rifleman"],
		["Operator","Rifleman"] 
    ],
    [
		["Rangemaster", "Rangemaster"]   
    ]
];
// Trim it down to the current playerside
switch (playerside) do {
    case West: { loadouts = loadouts select 0};
    case East: { loadouts = loadouts select 1};
    case Resistance: { loadouts = loadouts select 2};
    default { loadouts = loadouts select 3};
};

/*-----------------------------------------------------------------		*/
/* fill listbox*/
{
		lbAdd [PIXLOGISTIC_IDC_DIALOG_BARRACK_List, (_x select 0)];
} foreach loadouts;

/*-----------------------------------------------------------------	*/
/* Dialog anzeigen*/
pixLogisticDialogBarrack_ButtonOK = 0;
pixLogisticDialogBarrack_Selection = 0;


waituntil{sleep 0.1;!(isnull (finddisplay 850))};
_dummy = "B_Soldier_F" createVehicleLocal [(getPos player select 0) + 0, (getPos player select 1) + 0, (getPos player select 2) + 0];
this = _dummy;
lbSetCurSel [PIXLOGISTIC_IDC_DIALOG_BARRACK_List, 0];
_dummy setdir direction player;
player hideObject true; 
_cam = "camera" camcreate [0,0,0];
_cam cameraeffect ["internal", "back"] ;
_cam camsettarget _dummy;
_cam camsetrelpos [1.25,1.25,1.25];
//_cam camsettarget [(getpos player select 0),(getpos player select 1),(getpos player select 2)+1];
_cam camsettarget (_dummy modeltoworld [-3,0,0.75]);
_cam camcommit 0 ; 
showCinemaBorder  false;
	
if ((currentVisionMode player)==1) then { camUseNVG true;}
else {camUseNVG false;};

cameraEffectEnableHUD true;

_rot=1;
waituntil
{
	sleep 0.015;
	if (_rot>0) then {_dummy setdir ((direction _dummy)-0.20);};
	if (_rot<0) then {_dummy setdir ((direction _dummy)+0.20);};
	_rot=_rot+1;
	if (_rot>150) then {_rot=-150;};
	(isnull (finddisplay 850));
};

player hideObject false; 
deleteVehicle _dummy;
_cam cameraeffect ["terminate", "back"] ;
camdestroy _cam;
pixLogisticDialogBarrack_Selection = nil;